<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Per extends Model
{
    
    protected $table = 'per';
    protected $primaryKey = 'id';

    protected $fillable = [
    	'name', 'code', 'created_by', 'updated_by',
    ];

    protected $hidden = ['updated_at', 'created_at'];
}
