<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Inbox extends Model
{
    use SoftDeletes;
    protected $table = 'inbox';
    protected $primaryKey = 'id';

    protected $fillable = [
    	'message_id', 'from', 'title', 'message', 'status', 'created_by', 'updated_by',
    ];

    protected $dates = ['deleted_at'];
    protected $hidden = ['updated_at', 'created_at'];

    public static function owner_notification($status,$from){
        return DB::select("SELECT 
            COUNT(id) AS x 
        FROM inbox as a
        WHERE status = ? AND a.from != ? AND deleted_at IS NULL",
        [$status,$from])[0]->x;
    }

    public static function customer_notification($status,$from){
        return DB::select("SELECT 
            COUNT(id) AS x 
        FROM inbox as a
        WHERE status = ? AND a.from = ? AND deleted_at IS NULL",
        [$status,$from])[0]->x;
    }

}
