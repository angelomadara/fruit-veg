<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class OrderNumber extends Model
{
    use SoftDeletes;
    protected $table = 'order_number';
    protected $primaryKey = 'id';

    protected $fillable = [
    	'order_id', 'customer_id', 'delivery_date', 'status', 'created_by', 'updated_by',
    ];

    protected $dates = ['deleted_at'];
    protected $hidden = ['updated_at', 'created_at'];

    public static function ownerNotification(){
        return DB::select("SELECT 
            COUNT(id) AS x 
        FROM order_number AS ord 
        WHERE ord.status = ?
        ",
        ['pending'])[0]->x;
    }

    public static function customerNotification($id){
        return DB::select("SELECT 
            COUNT(id) AS x 
        FROM order_number AS ord 
        WHERE ord.status = ?
        AND ord.customer_id = ?
        ",
        ['confirmed',$id])[0]->x;
    }
}