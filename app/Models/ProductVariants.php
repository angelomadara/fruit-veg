<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;

class ProductVariants extends Model
{
    use SoftDeletes;
    protected $table = 'product_categories';
    protected $primaryKey = 'id';

    protected $fillable = [
    	'product_id', 'name', 'code', 'created_by', 'updated_by',
    ];

    protected $dates = ['deleted_at'];
    protected $hidden = ['updated_at', 'created_at'];
}
