<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductTypes extends Model
{
    use SoftDeletes;
    protected $table = 'product_categories';
    protected $primaryKey = 'id';

    protected $fillable = [
    	'name', 'code', 'note', 'created_by', 'updated_by',
    ];

    protected $dates = ['deleted_at'];
    protected $hidden = ['updated_at', 'created_at'];
}
