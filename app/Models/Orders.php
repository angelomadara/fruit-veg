<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
    use SoftDeletes;
    protected $table = 'customer_orders';
    protected $primaryKey = 'id';

    protected $fillable = [
    	'order_id', 'customer_id', 'delivery_date', 'product_id', 'need','price', 'measurement', 'created_by', 'updated_by',
    ];

    protected $dates = ['deleted_at'];
    protected $hidden = ['updated_at', 'created_at'];
}
