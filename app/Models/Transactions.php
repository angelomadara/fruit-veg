<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transactions extends Model
{
    use SoftDeletes;
    protected $table = 'transactions';
    protected $primaryKey = 'id';

    protected $fillable = [
    	'order_id', 'credit', 'payment_date',
    ];

    protected $dates = ['deleted_at'];
    protected $hidden = ['updated_at', 'created_at'];
}
