<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Products extends Model
{
    use SoftDeletes;
    protected $table = 'products';
    protected $primaryKey = 'id';

    protected $fillable = [
    	'category_id', 'name', 'photo', 'code', 'price', 'quantity', 'per', 'note', 'created_by', 'updated_by',
    ];

    protected $dates = ['deleted_at'];
    protected $hidden = ['updated_at', 'created_at'];
}
