<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Models\Inbox as Inbaks;

class Inbox extends Controller
{
    public function index(){
        // return strtotime(date('ymd'));
        $id = Sentinel::getUser()->id;
        return view('customer.inbox',[
            'messages' => Inbaks::where('from',$id)->orderBy('id','desc')->groupBy('message_id')->get()
        ]);
    }

    public function create(Request $get){
        
        $id = Sentinel::getUser()->id;
        $a = Inbaks::create([
            'message_id'=> strtotime(date('d')).date('his'),
            'from'      => $id,
            'message'   => $get->input('message'),
            'title'     => $get->input('title'),
        ]);

        return [
            'swal' => [
                'title' => $a ? 'Message sent' : 'Error while sending message',
                'type' => $a ? 'success' : 'error'
            ]
        ];
    }

    public function reply(Request $get){
        $id = Sentinel::getUser()->id;
        $message_id = $get->input('message_id');
        $a = Inbaks::create([
            'message_id'=> $message_id,
            'from'      => $id,
            'message'   => $get->input('message'),
        ]);

        return [
            'swal' => [
                'title' => $a ? 'Message sent' : 'Error while sending message',
                'type' => $a ? 'success' : 'error'
            ]
        ];
    }

    public function message($id){
        $user_id = Sentinel::getUser()->id;

        $tread = Inbaks::where('message_id',$id)->orderBy('created_at','desc')->get();
        
        $inbox_id = $tread[0]->id; /** last message */
        $id = $tread[0]->from;
        
        if($id != $user_id){
            Inbaks::where('id',$inbox_id)->update(['status'=>'read']);
        }

        return view('customer.message',[
            'id' => $id,
            'messages' => $tread
        ]);
    }

    public function remove($id){
        $a = Inbaks::where('message_id',$id)->delete();
        return [
          'swal'  => [
              'title' => $a ? 'Message removed' : 'Error encountered',
              'type' => ($a ? 'success' : 'error')
          ]
        ];
    }
}
