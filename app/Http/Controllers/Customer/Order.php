<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cookie;
use Response;
use Sentinel;
use DB;

use App\Http\Controllers\Sys;
use App\Models\ProductTypes;
use App\Models\ProductVariants;
use App\Models\Per;
use App\Models\Products;
use App\Models\Orders;
use App\Models\OrderNumber;
use App\Models\Transactions;

class Order extends Controller
{

    public function index(){
        return view('customer.order-form',[
            'products' => Products::select(
                'products.id','products.name','products.photo','products.description','products.price','products.note',
                'per.name as per','per.code as percode','product_categories.name as category'
            )->leftJoin('per','per.id','products.per')->leftJoin('product_categories','product_categories.id','products.category_id')->get()
        ]);
    }

    public function sendOrder(Request $get){
        
        $orders = json_decode($get->input('data'),true);
        $user = Sentinel::getUser();
        $date = date('Y-m-d',strtotime($get->input('date')));
        $order_id = $get->input('order_id');

        if($date < date('Y-m-d')){
            return [
                'swal' => [
                    'title' => 'You have entered passed date',
                    'type' => 'error'
                ]
            ];
        }

        if($order_id == "" || $order_id == null){
            $order_id = sprintf(date('y').'%05d', count(OrderNumber::withTrashed()->get()));
            OrderNumber::create([
                'order_id' => $order_id, 
                'customer_id' => $user->id, 
                'delivery_date' => $date, 
                'created_by' => $user->id, 
                'updated_by' => $user->id, 
            ]);

            foreach ($orders as $key => $value) {
                $id = $value['id'];
                $quantity = $value['quantity'];

                Orders::create([
                    'order_id' => $order_id, 
                    'customer_id' => $user->id, 
                    'delivery_date' => $date, 
                    'product_id' => $id, 
                    'need' => $quantity, 
                    'price' => Products::where('id',$id)->first()->price,
                    'measurement' => Products::select('per.code')->where('products.id',$id)->leftJoin('per','per.id','products.per')->first()->code, 
                    'created_by' => $user->id, 
                    'updated_by' => $user->id,
                ]);
            }
        }
        else{

            Orders::where('order_id',$order_id)->delete();
            OrderNumber::where('order_id',$order_id)->update([
                'delivery_date' => $date
            ]);

            foreach ($orders as $key => $value) {
                $id = $value['id'];
                $quantity = $value['quantity'];
                Orders::create([
                    'order_id' => $order_id, 
                    'customer_id' => $user->id, 
                    'delivery_date' => $date, 
                    'product_id' => $id, 
                    'price' => Products::where('id',$id)->first()->price,
                    'need' => $quantity, 
                    'measurement' => Products::select('per.code')->where('products.id',$id)->leftJoin('per','per.id','products.per')->first()->code, 
                    'created_by' => $user->id, 
                    'updated_by' => $user->id,
                ]);
            }
        }

        

        return [
            'swal' => [
                'title' => 'Order Completed',
                'type' => 'success'
            ]
        ];
    }

    public function orderId($id){
        $user_id = Sentinel::getUser()->id;
        
        $items = DB::select("SELECT o.*, p.name AS product, p.price
        FROM customer_orders AS o
        LEFT JOIN products AS p ON p.id = o.product_id
        WHERE o.customer_id = ?
        AND o.order_id = ?
        AND o.deleted_at IS NULL
        ",[$user_id,$id]);

        return view('customer.orders',[
            'order' => OrderNumber::where('order_id',$id)->first(),
            'items' => $items,
            'id' => $id,
            'trans' => Transactions::where('order_id',$id)->get()
        ]);
    }

    public function orderIdUpdate($id){
        $user_id = Sentinel::getUser()->id;
        $items = DB::select("SELECT o.*, p.name AS product, p.price, p.id as pid
            FROM customer_orders AS o
            LEFT JOIN products AS p ON p.id = o.product_id
            WHERE o.customer_id = ?
            AND o.order_id = ?
            AND o.deleted_at IS NULL
            ",[$user_id,$id]);
        
        return view('customer.orders-update',[
            'products' => Products::select(
                        'products.id','products.name','products.photo','products.description','products.price','products.note',
                        'per.name as per','per.code as percode','product_categories.name as category'
                        )->leftJoin('per','per.id','products.per')->leftJoin('product_categories','product_categories.id','products.category_id')->get(),
            'order' => OrderNumber::where('order_id',$id)->first(),
            'items' => $items,
            'id' => $id,
            'trans' => Transactions::where('order_id',$id)->get()
        ]);
    }

    public function cancelOrder(Request $get){
        $id = $get->input('id');
        $user = Sentinel::getUser();

        $updated = OrderNumber::where('id',$id)
            ->where('customer_id',$user->id)
            ->update([
                'status' => 'cancelled'
            ]);

        if($updated){
            return [
                'swal' => [
                    'title' => 'Order Cancelled',
                    'type' => 'success'
                ]
            ];
        }else{
            return [
                'swal' => [
                    'title' => 'Error occurred',
                    'type' => 'error'
                ]
            ];
        }
    }
}
