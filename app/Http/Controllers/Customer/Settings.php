<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use Sentinel;
use App\Models\Customer;

class Settings extends Controller
{
    public function index(){
        $user_id = Sentinel::getUser()->id;
        
        $customer = Customer::where('customer_id',$user_id)->first();

        return view('customer.settings',[
            'customer' => $customer
        ]);

    }

    public function form(){
        $user_id = Sentinel::getUser()->id;        
        $customer = Customer::where('customer_id',$user_id)->first();
        return view('customer.settings-register',[
            'customer' => $customer
        ]);
    }

    public function update(Request $get){

        if($get->input('agree') == null){
            return redirect()->back()->with('error','Agree that all information is provided is correct.');
        }

        $isValid = Validator::make($get->all(),[
            "agree"     => "required",
            "company"   => "required",
            "address"   => "required",
            "telephone" => "required",
            "mobile"    => "required",
            "contact"   => "required",
        ]);
        
        if($isValid->fails()){
            return redirect()->back()->with('error','Incomplete information, provide all the information that is needed.');
        }

        $id = Sentinel::getUser()->id;

        $ifFound = Customer::where('customer_id',$id)->first();

        if($ifFound){
            Customer::where('customer_id',$id)->update([
                'customer_id'   => $id, 
                'business_name' => $get->input('company'), 
                'address'       => $get->input('address'), 
                'telephone'     => $get->input('telephone'), 
                'mobile'        => $get->input('mobile'), 
                'contact_person'=> $get->input('contact'),  
                'updated_by'    => $id, 
            ]);
        }else{
            Customer::create([
                'customer_id'   => $id, 
                'business_name' => $get->input('company'), 
                'address'       => $get->input('address'), 
                'telephone'     => $get->input('telephone'), 
                'mobile'        => $get->input('mobile'), 
                'contact_person'=> $get->input('contact'), 
                'created_by'    => $id, 
                'updated_by'    => $id, 
            ]);
        }
        return redirect()->back()->with('success','Profile successfully updated');
    }

    public function updatePassword(Request $get){
        $p1 = $get->input('password1');
        $p2 = $get->input('password2');
        $id = Sentinel::getUser()->id;

        if($p1 === $p2){
            $user = Sentinel::findById($id);

            $credentials = [
                'password' => $p1,
            ];

            $user = Sentinel::update($user, $credentials);

            return [
                'swal' => [
                    'title' => 'Password successfully updated',
                    'type' => 'success'
                ]
            ];
        }
    }

    public function check(){
        $id = Sentinel::getUser()->id;
        $ifFound = Customer::where('customer_id',$id)->first();
        return [
            'isValid' => $ifFound ? true : false
        ];
    }
}
