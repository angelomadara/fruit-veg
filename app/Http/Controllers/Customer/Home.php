<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Sentinel;
use App\Models\Orders;
use App\Models\OrderNumber;

class Home extends Controller
{
    public function index(){
        return view('customer.index',[
            'orders' => OrderNumber::where('customer_id',Sentinel::getUser()->id)->orderBy("delivery_date","asc")->get()
        ]);
    }
}
