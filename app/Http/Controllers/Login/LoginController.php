<?php

namespace App\Http\Controllers\Login;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Activation;
use Mail;
use DB;
use Validator;

class LoginController extends Controller
{
    public function index(){
        if(Sentinel::check()){
            $foundUser = Sentinel::check();
            $role = $foundUser->roles()->first()->slug;
            if($role=="owner"){
                return redirect('/store');
            }
            elseif($role=="customer"){

            }
            elseif($role=="superuser"){

            }
            else{
                return view("forms.login");
            }
        }
        return view("forms.login");
    }

    public function register(){
        return view('forms.register');
    }

    public function addUser(Request $get){
        $email = $get->input('email');
        $username = $get->input('username');
        $password1 = $get->input('password1');
        $password2 = $get->input('password2');
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return redirect()->back()->with("warning","Invalid email");
        }
        // return $get->all();
        if($password1 === $password2){
            if(strlen($username) < 8){
                return redirect()->back()->with("warning","Username must be eight (8) characters long");
            }
            $credentials = [
                'email'    => $get->input('email'),
                'username' => $get->input('username'),
                'password' => $password1,
            ];        
            
            $user = Sentinel::register($credentials);
            $activation = Activation::create($user);

            Mail::send('email.registration',['activation'=>$activation], function($message) use($user){
                $message->from('receiver8080@gmail.com', 'Joel & Anale Store');
                $message->to($user->email, $user->username)->subject('Joel & Anale Store');
            });
            return redirect()->back()->with('success',"You've successfully registered check your email to complete your registration.");
        }else{
            return redirect()->back()->with('warning',"Passwords didn't matched.");
        }
    }

    public function activate($id,$code){
        /** get user by id */
        $user = Sentinel::findById($id);
        /** activate account */
        $x = Activation::complete($user, $code);
        /** if user has not in role of customer */
        if(!$user->inRole(Sentinel::findRoleBySlug('customer'))){
            $user->roles()->attach(Sentinel::findRoleBySlug('customer'));   
        }
        return view('email.complete',[
            'status' => $x ? "success" : "warning",
            'message' => $x ? "Activation completed" : "Activation error"
        ]);
    }

    public function forget(){
        return view("forms.forget");
    }

    public function reset(Request $get){
        
        $foundUser = DB::select("SELECT * FROM users WHERE username = ? OR email = ?",[$get->username,$get->username]);
        if($foundUser){
            $id = $foundUser[0]->id;
            return view('forms.update-password',['id'=>$id]);
        }else{
            return redirect()->back()->with('error','Sorry username/email cannot be found');
        }
    }

    public function updatePassword(Request $get){
        // return ;
        // $a = $get->validate([
        //     'password1' => 'required|min:8',
        // ]);

        if(strlen($get->password1) < 8){
            return redirect()->back()->with('error','Password must be 8 characters long');
        }else{
            $user = Sentinel::findById($get->id);
            Sentinel::update($user,['password' => $get->password1]);
            return redirect()->back()->with('success','Password successfully updated');
        }
    }

    public function authenticate(Request $get){
        
        $username = $get->input('username');
        $password = $get->input('password');

        if(strpos($username,'@') == false){  
            $credentials = [
                'username' => $username,
                'password' => $password,
            ];  
        }else{
            $credentials = [
                'email' => $username,
                'password' => $password,
            ];
        }

        $isTrue = Sentinel::authenticate($credentials);

        if($isTrue){
            if($isTrue->inRole('owner')){
                return redirect('store');
            }
            elseif($isTrue->inRole('customer')){
                return redirect('customer');
            }
            elseif($isTrue->inRole('superuser')){
                return "Super-user page cannot be access.";
            }else{
                return ":)";
            }
        }else{
            return redirect()->back()->with('error',"Incorrect credentials");
        }
    }


}
