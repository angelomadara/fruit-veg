<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Models\Inbox as Inbaks;
use App\Models\Customer;

class Inbox extends Controller
{
    public function index(){
    	return view('store.inbox',[
            'inbox' => Inbaks::select('inbox.*','customers.business_name','customers.telephone','customers.mobile','customers.address')
                            ->leftJoin('customers','customers.customer_id','inbox.from')
                            ->groupBy("message_id")->orderBy('created_at','desc')->get()
        ]);
    }

    public function messageId($id){
        $user_id = Sentinel::getUser()->id;
        $tread = Inbaks::where('message_id',$id)->orderBy('created_at','desc')->get();
        
        $inbox_id = $tread[0]->id; /** last message */
        $id = $tread[0]->from;
        
        if($id != $user_id){
            Inbaks::where('id',$inbox_id)->update(['status'=>'read']);
        }

        return view('store.message',[
            'tread' => $tread,
            'id' => $id
        ]);
    }

    public function reply(Request $get){
        $x = Inbaks::create([
            'message_id' => $get->id,
            'message'   => $get->message,
            'from' => Sentinel::getUser()->id
        ]);
        
        return [
            'swal' => [
                'title' => $x ? "Message sent" : "Error while sending message",
                'type' => $x ? "success" : "error"
            ]
        ];
    }

    public function remove($id){
        $a = Inbaks::where('message_id',$id)->delete();
        return [
          'swal'  => [
              'title' => $a ? 'Message removed' : 'Error encountered',
              'type' => ($a ? 'success' : 'error')
          ]
        ];
    }

    public function announcement(Request $get){
        $message = $get->message;
        $customers = Customer::get();
        if($customers){
            foreach ($customers as $customer){
                $a = Inbaks::create([
                    'message_id'=> strtotime(date('d')).date('his'),
                    'from'      => $customer->customer_id,
                    'message'   => $message,
                    'title'     => $get->title,
                ]);
            }
        }else{

        }
    }
}
