<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Sentinel;
use DB;

use App\Http\Controllers\Sys;
use App\Models\ProductTypes;
use App\Models\ProductVariants;
use App\Models\Per;
use App\Models\Products;
use App\Models\Orders;
use App\Models\OrderNumber;
use App\Models\Customer as Kustomer;
use App\Models\Transactions as Transaction;

class Customer extends Controller
{
    public function index(){        
        $orders = OrderNumber::select('order_number.*','c.business_name')
                        ->leftJoin('customers as c','c.customer_id','order_number.customer_id')
                        // ->where('status','!=','cancelled')
                        ->orderBy('order_id','desc')->get();

        return view("store.customer",[
            'orders' => $orders
        ]);
    }

    public function orders($id,$order){

        $customer = Kustomer::where('customer_id',$id)->first();
        $items = Orders::where('order_id',$order)->get();
        
        return view("store.customer-orders",[
            'order_id' => $order,
            'order' => OrderNumber::where('order_id',$order)->first(),
            'items' => $items,
            'customer' => $customer,
            'transactions' => Transaction::where('order_id',$order)->get()
        ]);
    }

    public function approve($order_id){
        $updated = OrderNumber::where('order_id',$order_id)->update([
            'status' => 'confirmed'
        ]);

        if($updated){
            return redirect()->back()->with('success','Status successfully updated');
        }else{
            return redirect()->back()->with('error','Error occured while updating status');
        }
    }
    public function credit(Request $get){
        $date       = $get->input('date');
        $credit     = str_replace(",","",$get->input('credit'));
        $order_id   = $get->input('order_id');
        $id         = Sentinel::getUser()->id;

        $grandTotal = 0;
        $creditTotal = 0;
        $items = Orders::where('order_id',$order_id)->get();
        foreach ($items as $key => $item) { 
            $grandTotal += $item->price * $item->need; }
        $trans = Transaction::where('order_id',$order_id)->get();
        foreach ($trans as $tran) { $creditTotal += $tran->credit; }
            
        $balance = $grandTotal - $creditTotal;
        if($balance <= $credit) {
            $credit = $balance;
        }

        $t = Transaction::create([
            'order_id'      => $order_id,
            'credit'        => $credit,
            'payment_date'  => date('Y-m-d',strtotime($date)),
            'created_by'    => $id,
            'updated_by'    => $id,
        ]);

        if($t){
            return [
                'swal' => [
                    'title' => 'Credit saved',
                    'type' => 'success'
                ]
            ];
        }else{
            return [
                'swal' => [
                    'title' => 'Server Error occured',
                    'type' => 'error'
                ]
            ];
        }
    }

    // public function credit(Request $get){

    //     $t = Transaction::create([
    //         'order_id' => $get->input('order_id'),
    //         'credit' => str_replace(",","",$get->input('credit')),
    //         'payment_date' => date('Y-m-d',strtotime($get->input('date'))),
    //         'created_by' => Sentinel::getUser()->id,
    //         'updated_by' => Sentinel::getUser()->id,
    //     ]);

    //     if($t){
    //         return [
    //             'swal' => [
    //                 'title' => 'Credit saved',
    //                 'type' => 'success'
    //             ]
    //         ];
    //     }else{
    //         return [
    //             'swal' => [
    //                 'title' => 'Server Error occured',
    //                 'type' => 'error'
    //             ]
    //         ];
    //     }
    // }

    public function delivered(Request $get){
        
        $t = OrderNumber::where('order_id',$get->id)->update([
            'status' => 'completed',
            'updated_by' => Sentinel::getUser()->id,
        ]);
            
        if($t){
            $prods = Orders::where('order_id',$get->id)->get();
            foreach($prods as $prod){
                $need = $prod->need;
                $stock = Products::where('id',$prod->product_id)->first();
                Products::where('id',$prod->product_id)->update([
                    'quantity' => ($stock->quantity - $need)
                ]);
            }
        }

        return [
            'swal' => [
                'title' => $t ? 'Order successfully delivered' : 'Server Error occured',
                'type' => $t ? 'success' : 'error'
            ]
        ];
    }
}
