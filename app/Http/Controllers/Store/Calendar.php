<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use DB;

use App\Http\Controllers\Sys;
use App\Models\ProductTypes;
use App\Models\ProductVariants;
use App\Models\Per;
use App\Models\Products;
use App\Models\Orders;
use App\Models\OrderNumber;
use App\Models\Customer as Kustomer;
use App\Models\Transactions;

class Calendar extends Controller
{
    public function index(){
    	return view('store.calendar');
    }

    public function getOrderCount(Request $get){
        $date = [];
        $start = $get->input('start');
        $end = $get->input('end');

        $orders = OrderNumber::select(
            'order_number.delivery_date','order_number.order_id','order_number.status',
            'order_number.customer_id','customers.business_name'
        )
        ->whereBetween('delivery_date',[$start,$end])
        ->leftJoin('customers','customers.customer_id','order_number.customer_id')
        ->get();

        foreach($orders as $order){
            $debit = $credit = 0;
            $os = Orders::where('order_id',$order->order_id)->get();
            foreach($os as $o){ $debit+=($o->price*$o->need); }
            $trans = Transactions::where('order_id',$order->order_id)->get();
            foreach($trans as $tran){ $credit+=$tran->credit; }

            $date[] = [
                'start' => $order->delivery_date,
                'title' => "<a href='/store/customer/order/".$order->customer_id."/".$order->order_id."' style='color:#fff;'>".
                    "Customer : ".$order->business_name.
                    "<br>&nbsp;&nbsp;Status : ".$order->status."</a>".
                    "<div style='display:inline-block;margin-left:5px;height:10px;width:10px;border-radius:100%;".(($debit-$credit)<1&&$order->status!='pending'?'background:green;':'background:red;')."'></div>"
                    
            ];
        }

        return $date;
    }
}
