<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use DB;

use App\Http\Controllers\Sys;
use App\Models\ProductTypes;
use App\Models\ProductVariants;
use App\Models\Per;
use App\Models\Products;
use App\Models\Orders;
use App\Models\OrderNumber;
use App\Models\Transactions;
use App\Models\Customer as Kustomer;

class Grap extends Controller
{
    public function index(){
    	return view('store.grap');
    }

    public function sales(){
        $y = [];
        $year = date("Y");

        for($x = 1; $x <= 12; $x++){
            $start = $year.'-'.$x.'-01';
            $end = date("Y-m-t",strtotime($start));
            $ordNums = OrderNumber::whereBetween('delivery_date',[$start,$end])->get();
            $grandTotal = $totalPaid = 0;
            foreach($ordNums as $ordNum){
                $cos = Orders::where('order_id',$ordNum->order_id)->get();
                $totPaid = Transactions::where('order_id',$ordNum->order_id)->get();
                $coTotal = $totPayment = 0;
                foreach($cos as $co){
                    $coTotal += ($co->price * $co->need);
                }
                foreach($totPaid as $tot){
                    $totPayment += $tot->credit;
                }
                $grandTotal += $coTotal;
                $totalPaid += $totPayment;
            }
            $y[] = $grandTotal;
            $z[] = ($totalPaid > $grandTotal ? $grandTotal : $totalPaid);
        }
        return ['average'=>$y,'paid'=>$z];
    }
}
