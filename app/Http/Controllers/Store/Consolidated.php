<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Sentinel;
use DB;

use App\Http\Controllers\Sys;
use App\Models\ProductTypes;
use App\Models\ProductVariants;
use App\Models\Per;
use App\Models\Products;
use App\Models\Orders;
use App\Models\OrderNumber;
use App\Models\Customer as Kustomer;

class Consolidated extends Controller
{
    public function orders(Request $get){
        $products = Products::select("products.name", "products.id","per.code")
                    ->leftJoin("per","per.id","products.per")->get();
        $date = date('Y-m-d',strtotime($get->input('date')));

        $combine = [];

        foreach($products as $product){
            $sum = Orders::select(DB::raw("SUM(customer_orders.need) AS x"))
            ->leftJoin('order_number as onum','onum.order_id','customer_orders.order_id')
            ->where("customer_orders.delivery_date",$date)->where("customer_orders.product_id",$product->id)
            // ->where('onum.status','confirmed')
            ->first()->x;
            if($sum){
                $combine[] = [
                    'name' => $product->name,
                    'sum' => ($sum ? $sum : 0) ." ".$product->code,
                ];
            }   
        }
        // return $combine;
        return view('store.consolidated',[
            'date' => $date,
            'combine' => $combine
        ]);
        
    }
}
