<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Sentinel;
use DB;

use App\Http\Controllers\Sys;
use App\Models\ProductTypes;
use App\Models\ProductVariants;
use App\Models\Per;
use App\Models\Products;
use App\Models\Orders;
use App\Models\OrderNumber;
use App\Models\Customer;
use App\Models\Transactions;

class Invoice extends Controller
{
    public function index(){
        $customers = Customer::get();

        $final = [];

        foreach ($customers as $key => $customer) {
            $orders = [];
            $total = $credit =0;
            $customer_orders = OrderNumber::whereIn('status',['confirmed','delivered'])->where('customer_id',$customer->customer_id)->get();
            
            if(count($customer_orders) > 0){
                $c = [];
                foreach ($customer_orders as $order) {
                    $total = $credit =0;
                    $xx = Orders::where('order_id',$order->order_id)->get();
                    foreach($xx as $x){ $total+=$x->need * $x->price; }
                    $tt = Transactions::where('order_id',$order->order_id)->get();
                    foreach($tt as $t){ $credit+=$t->credit; }

                    if($credit == 0 || $credit < $total){
                        $c[] = [
                            'order_id' => $order->order_id,
                            'customer_id' => $customer->id,
                            'total' => $total,
                            'credit' => $credit
                        ];
                    }
                }

                if(count($c) > 0){
                    $final[] = [
                        'business_name' => $customer->business_name,
                        'customer_id' => $customer->customer_id,
                        'orders' => $c
                    ];  
                }
            }

        }
        
        return view('store.singilan',[
            'customers' => $final
        ]);
    }

    
    public function invoiceSamaSama($id){
        
        $items = explode(',',$id);    
        array_pop($items); /** remove the last item of the array */
        
        // $order = Orders::where('order_id',$items)->get();

        $customer = OrderNumber::whereIn('order_id',$items)
                    ->leftJoin('customers','customers.customer_id','order_number.customer_id')
                    ->get();
        
        // return $customer[0]->business_name;

        return view('store.invoice2',[
            'customer' => $customer,
            'order_id' => $items,
            // 'orders' => $order,
            // 'transactions' => Transactions::where('order_id',$order_id)->get()
        ]);

    }
}
