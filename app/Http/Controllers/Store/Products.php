<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use Image;

use App\Http\Controllers\Sys;
use App\Models\ProductTypes;
use App\Models\ProductVariants;
use App\Models\Per;
use App\Models\Products as Produkto;

class Products extends Controller
{
    public function index(){
        return view('store.products',[
            'products' => Produkto::get(),
        ]);
    }

    public function form(){
        return view('forms.product-form',[
            'types' => ProductTypes::get(),
            'per' => Per::get(),
        ]);
    }

    public function create(Request $get){
        // return $get->all();
        if(Validator::make($get->all(),[
            "name"      => 'required',
            "code"      => 'nullable',
            "category"  => 'required',
            "price"     => 'required',
            "per"       => 'required',
            "note"      => 'nullable',
            "image"     => 'image|required'
        ])->fails()){
            return redirect()->back()->with('error','All labels with asterisk(*) are important');
        }

        /** image manipulation */
        $image_name = Sys::randName(30);
        $file 			= $get->file('image');
		$file_ext_name 	= strtolower($file->getClientOriginalExtension());
		$file_size 		= $file->getClientSize();
        $file_mime 		= $file->getMimeType();
        $image_name     = $image_name.".".$file_ext_name;
        Image::make($file)->resize(400, null,function($constraint){
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save(public_path('/images/small/'.$image_name));
        Image::make($file)->resize(800, 700,function($constraint){
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save(public_path('/images/big/'.$image_name));
        $isTrue = Produkto::create([
            'category_id' => $get->input('category'), 
            'name'      => $get->input('name'), 
            'photo'     => $image_name,
            'code'      => $get->input('code'), 
            'price'     => $get->input('price'), 
            'per'       => $get->input('per'), 
            'note'      => $get->input('note'), 
            'created_by'=> 0, 
            'updated_by'=> 0,
        ]);

        if($isTrue){
            return redirect()->back()->with('success','Product successfully saved');
        }else{
            return redirect()->back()->with('error','Error occured while saving the new product');
        }        
    }

    public function update(Request $get,$id){
        if(Validator::make($get->all(),[
            "name"      => 'required',
            "code"      => 'nullable',
            "category"  => 'required',
            "price"     => 'required',
            "per"       => 'required',
            "note"      => 'nullable',
            // "image"     => 'image|required'
        ])->fails()){
            return redirect()->back()->with('error','All labels with asterisk(*) are important');
        }
        /** image manipulation */
        $img = false;
        if($get->file('image')){
            $image_name = Sys::randName(30);
            $file 			= $get->file('image');
            $file_ext_name 	= strtolower($file->getClientOriginalExtension());
            $file_size 		= $file->getClientSize();
            $file_mime 		= $file->getMimeType();
            $image_name     = $image_name.".".$file_ext_name;
            Image::make($file)->resize(400, null,function($constraint){
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save(public_path('/images/small/'.$image_name));
            Image::make($file)->resize(800, 700,function($constraint){
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save(public_path('/images/big/'.$image_name));
            $img = Produkto::where('id',$id)->update([
                'photo'     => $image_name,
            ]);
        }
        $isTrue = Produkto::where('id',$id)->update([
            'category_id' => $get->input('category'), 
            'name'      => $get->input('name'), 
            // 'photo'     => $image_name,
            'code'      => $get->input('code'), 
            'price'     => $get->input('price'), 
            'per'       => $get->input('per'), 
            'note'      => $get->input('note'), 
        ]);
        if($isTrue || $img){
            return redirect()->back()->with('success','Product successfully updated');
        }else{
            return redirect()->back()->with('error','Error occured while updating the product');
        } 
    }

    public function quantity(Request $get){
        // return $get->all();
        $id = $get->input('id');
        $q = $get->input('quantity');
        $product = Produkto::where('id',$get->input('id'))->first();

        $x = Produkto::where('id',$id)->update([
            'quantity'=> ($q + $product->quantity)
        ]);
            
        return [
            'swal' => [
                'title' => $x ? "Product quantity updated" : "Error while updating quantity",
                'type' => $x ? "success" : "error"
            ]
        ];
    }

    public function remove($id){
        $a = Produkto::where('id',$id)->delete();
        return [
          'swal'  => [
              'title' => $a ? 'Product successfully removed' : 'Error while removing product',
              'type' => ($a ? 'success' : 'Error')
          ]
        ];
        // return redirect()->back()->with('success','Product has been removed.');
    }
}
