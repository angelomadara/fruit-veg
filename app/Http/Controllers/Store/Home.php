<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use DB;

use App\Http\Controllers\Sys;
use App\Models\ProductTypes;
use App\Models\ProductVariants;
use App\Models\Per;
use App\Models\Products;
use App\Models\Orders;
use App\Models\OrderNumber;
use App\Models\Customer as Kustomer;
use App\Models\Transactions as Transaction;

class Home extends Controller
{
    public function index(){
        if(!Sentinel::check()){
            return view("forms.login");
        }

        $thismonday = date("Y-m-d", strtotime('monday this week'));
        $thissunday = date("Y-m-d", strtotime('sunday this week'));

        $nextmonday = date("Y-m-d", strtotime('monday next week'));
        $nextsunday = date("Y-m-d", strtotime('sunday next week'));

    	return view('store.home',[
            'title' => 'Home',
            'this_week_count' => OrderNumber::whereBetween('created_at',[$thismonday, $thissunday])->count(),
            'next_week_count' => OrderNumber::whereBetween('delivery_date',[$nextmonday, $nextsunday])->count(),
            'thismonday' => date("M/d/y",strtotime($thismonday)),
            'thissunday' => date("M/d/y",strtotime($thissunday)),
            'nextmonday' => date("M/d/y",strtotime($nextmonday)),
            'nextsunday' => date("M/d/y",strtotime($nextsunday)),
        ]);
    }

    public function thisWeek(){
        $monday     = date("Y-m-d", strtotime('monday this week'));
        $tuesday    = date("Y-m-d", strtotime('tuesday this week'));
        $wednesday  = date("Y-m-d", strtotime('wednesday this week'));
        $thursday   = date("Y-m-d", strtotime('thursday this week'));
        $friday     = date("Y-m-d", strtotime('friday this week'));
        $saturday   = date("Y-m-d", strtotime('saturday this week'));
        $sunday     = date("Y-m-d", strtotime('sunday this week'));

        return [
            [
                date("d",strtotime('monday this week')),
                OrderNumber::where('created_at','like',$monday.'%')->count()
            ],
            [
                date("d",strtotime('tuesday this week')),
                OrderNumber::where('created_at','like',$tuesday.'%')->count()
            ],
            [
                date("d",strtotime('wednesday this week')),
                OrderNumber::where('created_at','like',$wednesday.'%')->count()
            ],
            [
                date("d",strtotime('thursday this week')),
                OrderNumber::where('created_at','like',$thursday.'%')->count()
            ],
            [
                date("d",strtotime('friday this week')),
                OrderNumber::where('created_at','like',$friday.'%')->count()
            ],
            [
                date("d",strtotime('saturday this week')),
                OrderNumber::where('created_at','like',$saturday.'%')->count()
            ],
            [
                date("d",strtotime('sunday this week')),
                OrderNumber::where('created_at','like',$sunday.'%')->count()
            ],
        ];

    }
}
