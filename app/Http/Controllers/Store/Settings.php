<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;

class Settings extends Controller
{
    public function index(){
    	return view('store.settings');
    }

    public function updatePassword(Request $get){
        $p1 = $get->p1;
        $p2 = $get->p2;

        $user = Sentinel::findById(Sentinel::getUser()->id);
        $x = false;
        if(($p1 === $p2) && (strlen($p1) >= 8 || strlen($p2) >= 8)){
            $x = Sentinel::update($user,[
                'password' => $p1
            ]);
        }else{
            $x = false;
        }

        return [
            'swal' => [
                'title' => $x ? 'Password successfully updated' : 'Error while updating password',
                'type' => $x ? 'success' : 'error'
            ]
        ];
    }
}
