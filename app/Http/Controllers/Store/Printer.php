<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Sentinel;
use DB;

use App\Http\Controllers\Sys;
use App\Models\ProductTypes;
use App\Models\ProductVariants;
use App\Models\Per;
use App\Models\Products;
use App\Models\Orders;
use App\Models\OrderNumber;
use App\Models\Customer as Kustomer;
use App\Models\Transactions;

class Printer extends Controller
{
    public function order($order_id){

        $order = Orders::where('order_id',$order_id)->get();
        // $order = DB::select("SELECT o.*, p.name, p.price
        // FROM customer_orders AS o
        // LEFT JOIN products AS p ON p.id = o.product_id
        // WHERE o.order_id = ?
        // AND o.deleted_at IS NULL
        // ",[$order_id]);

        $customer = OrderNumber::where('order_id',$order_id)
                    ->leftJoin('customers','customers.customer_id','order_number.customer_id')
                    ->first();
        
        return view('store.invoice',[
            'customer' => $customer,
            'order_id' => $order_id,
            'orders' => $order,
            'transactions' => Transactions::where('order_id',$order_id)->get()
        ]);
    }

    public function consolidated(Request $get){
        $date = date("Y-m-d",strtotime($get->input('date')));

        $products = Products::select("products.name", "products.id","per.code")
                    ->leftJoin("per","per.id","products.per")->get();

        $combine = [];

        foreach($products as $product){
            $sum = Orders::select(DB::raw("SUM(customer_orders.need) AS x"))
            ->leftJoin('order_number as onum','onum.order_id','customer_orders.order_id')
            ->where("customer_orders.delivery_date",$date)->where("customer_orders.product_id",$product->id)
            // ->where('onum.status','confirmed')
            ->first()->x;
            if($sum){
                $combine[] = [
                    'name' => $product->name,
                    'sum' => ($sum ? $sum : 0) ." ".$product->code,
                ];
            }   
        }
        return view('store.print-consolidated',[
            'date'  => date("M d, Y",strtotime($date)),
            'orders' => $combine
        ]);
    }
}
