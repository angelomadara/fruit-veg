<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Mail;

class ContactController extends Controller
{
    public function index(){
        return view('page.contact');
    }

    public function sendEmail(Request $get){
        $contact = $get->all();
        $email = $get->email;
        $name = $get->name;
        $asd = [$email,$name];

        if(!$email) return redirect()->back()->with('error',"Error occured while sending message");

        $x = Mail::send('email.contact',['contact'=>$contact], function($message) use($asd){
            $message->from($asd[0], 'Inquiry');
            $message->to('sstginquiry@mailinator.com', $asd[1])->subject('Inquiry');
        });

        return redirect()->back()->with('success',"Inquiry sent");
        
    }
}
