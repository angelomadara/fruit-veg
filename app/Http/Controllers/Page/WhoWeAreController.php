<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WhoWeAreController extends Controller
{
    public function index(){
        return view('page.team');
    }
}
