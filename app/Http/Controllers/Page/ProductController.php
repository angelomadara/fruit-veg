<?php

namespace App\Http\Controllers\Page;

use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products as Produkto;

class ProductController extends Controller
{
    public function index(){

        $p = DB::select("SELECT p.id, p.name, p.photo, p.description, p.price, p.note, e.code
            FROM products AS p
            LEFT JOIN per AS e ON e.id = p.per
            WHERE p.deleted_at IS NULL
        ");

        return view('page.products',[
            'products' => $p
        ]);
    }
}
