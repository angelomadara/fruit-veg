<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class UserRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $found_role = array_slice(func_get_args(),2);
        if(Sentinel::check()){
            $slug = Sentinel::getUser()->roles()->first()->slug;
            if(in_array($slug, $found_role)){
                return $next($request);
            }
        }
        return back();
    }
}
