<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_number', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_id');
            $table->integer('customer_id')->comment('user id');
            $table->date('delivery_date');
            $table->string('status')->default('pending')->comment('pending,confirmed,completed');

            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_number');
    }
}
