String.prototype.pera = function(prefix,decimals){
	var prefix = prefix == undefined ? "PHP" : prefix;
	var decimals = decimals == undefined ? 2 : decimals;
	number = parseFloat(this).toFixed(decimals);
	number = number.toString().replace(/\,/g,'');
	return prefix+" "+number.toString().split('').reverse().join('').replace(/(\d{3}(?!.*\.|$))/g, '$1,').split('').reverse().join('');
}

function kwarta(xxx,prefix,decimals){
	var prefix = prefix == undefined ? "PHP" : prefix;
	var decimals = decimals == undefined ? 2 : decimals;
	number = parseFloat(xxx).toFixed(decimals);
	number = number.toString().replace(/\,/g,'');
	return prefix+" "+number.toString().split('').reverse().join('').replace(/(\d{3}(?!.*\.|$))/g, '$1,').split('').reverse().join('');
}