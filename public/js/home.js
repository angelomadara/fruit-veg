$(document).ready(function () {
    $.get('/store/report/week',function(week){
        var graphData = [{
                // Visits
                label: ' Weekly Monitor of '+moment(moment()).format("MMMM"),
                data: week,
                color: '#999999'
            }
        ];
        $.plot($('#graph-lines'), graphData, {
            series: {
                points: {
                    show: true,
                    radius: 5
                },
                lines: {
                    show: true
                },
                shadowSize: 0
            },
            label: {
                show: 1,
            },
            grid: {
                color: '#7f8c8d',
                borderColor: 'transparent',
                borderWidth: 20,
                hoverable: true
            },
            xaxis: {
                // tickColor: 'transparent',
                tickDecimals: 0,
            },
            yaxis: {
                tickSize: 2,
                color: 'red',
                labelWidth: 0
            }
        });
    
    });

    $('#graph-bars').hide();

    $('#lines').on('click', function (e) {
        $('#bars').removeClass('active');
        $('#graph-bars').fadeOut();
        $(this).addClass('active');
        $('#graph-lines').fadeIn();
        e.preventDefault();
    });

    $('#bars').on('click', function (e) {
        $('#lines').removeClass('active');
        $('#graph-lines').fadeOut();
        $(this).addClass('active');
        $('#graph-bars').fadeIn().removeClass('hidden');
        e.preventDefault();
    });

    // Tooltip #################################################
    function showTooltip(x, y, contents) {
        $('<div id="tooltip" style="background:yellow;color:#444">' + contents + '</div>').css({
            top: y - 16,
            left: x + 20
        }).appendTo('body').fadeIn();
    }

    var previousPoint = null;

    $('#graph-lines, #graph-bars').bind('plothover', function (event, pos, item) {
        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;
                $('#tooltip').remove();
                var x = item.datapoint[0],
                    y = item.datapoint[1];
                    showTooltip(item.pageX, item.pageY, y + ' orders on '+moment(moment()).format("MMMM")+" "+ x);
            }
        } else {
            $('#tooltip').remove();
            previousPoint = null;
        }
    });

});