$(function() {

    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }

    $(document)
        .on('keyup','.money-format',function(e){
            var number = $(this).val();
            number = number.toString().replace(/\,/g,'');
            $(this).val(number.toString().split('').reverse().join('').replace(/(\d{3}(?!.*\.|$))/g, '$1,').split('').reverse().join(''));
            // $(this).val(number);
        }).on('keydown','.money-format',function(e){
            if((e.keyCode>=48 && e.keyCode<=57)||(e.keyCode>=96 && e.keyCode<=105)||(e.keyCode==8)||(e.keyCode==46)||(e.keyCode==35)||(e.keyCode==36)||(e.keyCode==37)||(e.keyCode==39)||(e.keyCode==9)||(e.keyCode==110)||(e.keyCode==189)||(e.keyCode==190)){
            }else{e.preventDefault();}
        }).on('click','.money-format',function(e){ if($(this).val()==0){$(this).val("");} $(this).putCursorAtEnd();})
    ;
});

jQuery.fn.putCursorAtEnd = function() {
	return this.each(function() {    
		// Cache references
		var $el = $(this),
		el = this;
		// Only focus if input isn't already
		if (!$el.is(":focus")) {
			$el.focus();
		}
		// If this function exists... (IE 9+)
		if (el.setSelectionRange) {
			// Double the length because Opera is inconsistent about whether a carriage return is one character or two.
			var len = $el.val().length * 2;      
			// Timeout seems to be required for Blink
			setTimeout(function() {
			el.setSelectionRange(len, len);
			}, 1);    
		} else {      
			// As a fallback, replace the contents with itself
			// Doesn't work in Chrome, but Chrome supports setSelectionRange
			$el.val($el.val());      
		}
		// Scroll to the bottom, in case we're in a tall textarea
		// (Necessary for Firefox and Chrome)
		this.scrollTop = 999999;
	});
};

function popupWindow(url, title, w, h) {
	// Fixes dual-screen position Most browsers Firefox
	var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
	var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	var top = ((height / 2) - (h / 2)) + dualScreenTop;
	// console.log({
	// 	'left': left,
	// 	'top': top,
	// 	'width': width,
	// 	'height': height,
	// });
    var newWindow = window.open(url, title, 'directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,resizable=no, scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    // newWindow.document.write('<html><head><title></title>');
    // newWindow.document.write('<link rel="stylesheet" href="css/invoice.css" type="text/css" />');
    // newWindow.document.write('</head><body >');
    // newWindow.document.write(data);
    // newWindow.document.write('</body></html>');
    // newWindow.document.close();
    // newWindow.focus();
    // setTimeout(function(){newWindow.print();},500);
    // newWindow.close();

	// window.open('/pageaddress.html','winname',"directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=no,resizable=no,width=400,height=350");
	// Puts focus on the newWindow
	if (window.focus) {
		newWindow.focus();
    }
    
    return true;

}