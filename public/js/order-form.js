
(function(){
    $(document)
    .ready(function(){
        $(".datatable").DataTable({
            "order" : [[ 1 , 'asc' ]],
            "ordering": [[ 0, false ]]
        });
    })
    .on('click','.add-to-cart,.sub-to-cart',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        var name = $(this).closest('td').siblings('td.table-text').children('h6').text();
        var price = parseFloat($(this).closest('td').siblings('td.table-text').find('.presyo').text().replace(/,/g, ''));
        var table = $("#table-cart");
        
        var operator = $(this).data('operator');

        var id = $(this).parent("div").data('id');
        var per = $(this).parent("div").data('per');

        /** if the product id is found on the cart **/
        var isFound = table.find('tbody').children('tr[data-id="'+id+'"]');
        
        if(isFound.length == 0 && operator != "sub"){
            table.find('tbody').append("<tr data-id='"+id+"' data-qty='1'>"+
                "<td>1 "+per+"-"+name+"</td>"+
                "<td>PHP "+price+
                    "<input type='hidden' value='"+parseFloat(price)+"' class='input-price'>"+
                    "<input type='hidden' value='1' class='input-quantity'>"+
                "</td>"+
            "</tr>");
        }else{
            var tr = table.find("tbody > tr[data-id='"+id+"']");
            var quantity = parseInt(tr.find("input[class='input-quantity']").val());

            if(quantity == 1 && operator == 'sub'){ tr.remove(); }

            if(operator == "add") quantity = ++quantity;
            else quantity = --quantity;

            var presyo = parseFloat(price*quantity);
            
            tr.html(
                "<td>"+quantity+" "+per+"-"+name+"</td>"+
                "<td>"+kwarta(presyo)+
                    "<input type='hidden' value='"+parseFloat(presyo)+"' class='input-price'>"+
                    "<input type='hidden' value='"+quantity+"' class='input-quantity'>"+
                "</td>"
            );
        }

        var inputPrice = $(".input-price");
        var grandTotal = 0;
        $.each(inputPrice,function(i,value){
            grandTotal += parseFloat($(this).val());
        });

        table.find('tfoot').html("<tr>"+
            "<td><b>TOTAL:</b></td>"+
            "<td><b>"+kwarta(grandTotal)+"</b></td>"+
        "</tr>");
        
    })

    .on('click','#send-order',function(e){
        var tr = $("#table-cart > tbody > tr");
        var products = [];
        if(tr.length <= 0){ return;}
        swal({
            title: 'Date of needed<br> Ex. 01/23/2018',
            input: 'text',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            focusConfirm: true,
            onOpen: () => {
                $(document).find('.swal2-input').blur().datepicker({
                    language: 'en',
                    autoClose: true,
                    minDate: new Date(),
                    position: "bottom left"
                });
            },
            showLoaderOnConfirm: true,preConfirm: (text) => {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        var date = text;
                        var isValid = moment(date, ['MM/DD/YYYY',"YYYY/MM/DD","MM-DD-YYYY","YYYY-MM-DD"],true).isValid();                    
                        if (!isValid) {
                            swal.showValidationError('Invalid date')
                        }
                        resolve()
                    }, 500)
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {
            if(result.value){
                $.each(tr,function(i,value){
                    var quantity = $(this).find("input.input-quantity").val();
                    products[i] = {
                        "id":$(this).data('id'),
                        "quantity":quantity
                    };
                });
        
                $.post("/customer/send-order",{
                    data: JSON.stringify(products),
                    date: result.value,
                    _token: $(this).siblings("input[name='_token']").val(),
                    order_id : $("input[name='order_id']").val()
                },function(data){
                    if(data.swal.type == 'success'){
                        swal(data.swal).then(function(isOkay){
                            if(isOkay) location.reload(true);
                        });
                    }else{
                        swal(data.swal);
                    }
                });
            }
        });

        return;
        
    })
;
}());

