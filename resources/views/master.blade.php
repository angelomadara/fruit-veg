<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE HTML>
<html>
<head>
<title>@yield('title')- Joel &amp; Anale Online Ordering System  </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="{{ url('css/bootstrap.min.css') }}" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="{{ url('css/style.css') }}" rel='stylesheet' type='text/css' />
<link href="{{ url('css/font-awesome.css') }}" rel="stylesheet"> 
<link href="{{ url('css/animations.css') }}" rel="stylesheet"> 
<link rel="stylesheet" href="{{ url('bower/air-datepicker/dist/css/datepicker.css') }}"/>
<!-- <link rel="stylesheet" href="{{ url('bower/datatables.net-dt/css/jquery.dataTables.css') }}"/> -->
<link rel="stylesheet" href="{{ url('bower/datatables.net-bs/css/dataTables.bootstrap.min.css') }}"/>
<script src="{{ url('js/jquery.min.js') }}"> </script>
<!-- <script src="{{ url('js/moment.min.js') }}"> </script> -->
<script src="{{ url('bower/moment/min/moment.min.js') }}"> </script>
<!-- Mainly scripts -->
<script src="{{url('js/jquery.metisMenu.js')}}"></script>
<script src="{{url('js/jquery.slimscroll.min.js')}}"></script>
<script src="{{url('js/sweetalert2.all.js')}}"></script>
<!-- Custom and plugin javascript -->
<link href="{{url('css/custom.css')}}" rel="stylesheet">
<link href="{{url('css/system.css')}}" rel="stylesheet">
<script src="{{url('js/custom.js')}}"></script>
<script src="{{url('js/screenfull.js')}}"></script>
<script src="{{ url('bower/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('bower/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{url('bower/air-datepicker/dist/js/datepicker.js')}}"></script>
<script src="{{url('bower/air-datepicker/dist/js/i18n/datepicker.en.js')}}"></script>
<script>$(function(){$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);if(!screenfull.enabled){return false;}$('#toggle').click(function(){screenfull.toggle($('#container')[0]);});});</script>
@yield('css')
</head>
<body>
<div id="wrapper">

	@yield('navs')
        
    <div id="page-wrapper" class="gray-bg dashbard-1">
       	
       	@yield('content')
		
		<!-- <div class="copy">
            
	    </div> -->
		<div class="clearfix"> </div>
   </div>
</div>
<!---->
<!--scrolling js-->

	<!-- <script src="{{url('js/jquery.nicescroll.js')}}"></script> -->
	<script src="{{url('js/scripts.js')}}"></script>
	<!--//scrolling js-->
	<script src="{{url('js/bootstrap.min.js')}}"> </script>
	<script>  </script>
	@yield('js')
</body>
</html>

