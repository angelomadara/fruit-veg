
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title> Register </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="{{ url('css/bootstrap.min.css') }}" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="{{ url('css/style.css') }}" rel='stylesheet' type='text/css' />
<link href="{{ url('css/font-awesome.css') }}" rel="stylesheet"> 
<script src="{{ url('js/jquery.min.js') }}"> </script>
<script src="{{ url('js/bootstrap.min.js') }}"> </script>
</head>
<body>
	<div class="login">
		<h1>J&amp;A Online Ordering System</h1>
		<div class="login-bottom row">
			<h2>Activation</h2>
			<div class="col-lg-12">
				<div class='alert alert-{{$status}}'> {{ $message }} </div>
				<div class="float-right">
					<a href="{{ url('login') }}">Login</a>
				</div>
			</div>
		</div>
	</div>
		<!---->
<div class="copy-right">
            <p> &copy; 2016 Minimal. All Rights Reserved | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>	    </div>  
<!---->
<!--scrolling js-->
	<script src="{{ url('js/scripts.js"></script>
	<!--//scrolling js-->
</body>
</html>

