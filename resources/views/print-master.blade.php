<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ url('css/bootstrap.min.css') }}" rel='stylesheet' type='text/css' />
    <title></title>

    <!-- Styles -->
    <link href="{{ asset('css/invoice.css') }}" rel="stylesheet">
</head>
<body>

@yield('content')

<script>
window.print()
</script>
</body>
</html>
