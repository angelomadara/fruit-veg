<nav class="navbar-default navbar-static-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
       	<h1> <a class="navbar-brand" href="index">J & A</a></h1>         
	</div>
	<div class=" border-bottom">

    	<div class="full-left">
    	 	<section class="full-top">
				<button id="toggle"><i class="fa fa-arrows-alt"></i></button>	
			</section>
			<!-- <form class=" navbar-left-right">
          		<input type="text"  value="Search..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search...';}">
          		<input type="submit" value="" class="fa fa-search">
        	</form> -->
        	<div class="clearfix"> </div>
       	</div>

    <!-- Brand and toggle get grouped for better mobile display -->
 
   <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="drop-men" >
	        <ul class=" nav_1">
				@php 
					if(Sentinel::check()){
						$user = Sentinel::check()->roles()->first()->slug;
						$id = Sentinel::getUser()->id;
						if( $user == 'owner'){
							$notifs =  App\Models\OrderNumber::ownerNotification();
							$messages = App\Models\Inbox::owner_notification('unread',$id);
						}else{
							$messages = App\Models\Inbox::owner_notification('unread',$id);
							$notifs =  App\Models\OrderNumber::customerNotification(Sentinel::getUser()->id);
						}
					}
				@endphp
			   <!-- Notification -->
	    		<li class="dropdown at-drop">
	              	<a href="#" class="dropdown-toggle dropdown-at " data-toggle="dropdown">
	              		<i class="fa fa-globe"></i> 
						<span class="number"> {{ $notifs+$messages }} </span>
	              	</a>
	              	<ul class="dropdown-menu menu1 " role="menu">
	                	<li><a href="{{ url('store/customer/orders') }}">             
		                	<div class="user-new">
								<p>{{ $notifs }} {{ $user == 'owner' ? 'new customer orders' : 'confirmed orders' }} </p>
								<!-- <span>40 seconds ago</span> -->
		                	</div>
		                	<div class="clearfix"> </div>
	                	</a></li>

						<li><a href="{{ url('store/customer/inbox') }}">             
		                	<div class="user-new">
								<p>{{ $messages }} {{ $user == 'owner' ? 'new customer messages' : 'new messages' }} </p>
								<!-- <span>40 seconds ago</span> -->
		                	</div>
		                	<div class="clearfix"> </div>
	                	</a></li>
	              </ul>
	            </li>
				<li class="dropdown">
	              	<a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown">
	              		<!-- <span class=" name-caret">User<i class="caret"></i></span> -->
	              		<img src="{{ url('images/wo.jpg') }}">
	              	</a>
	              	<!-- <ul class="dropdown-menu " role="menu">
		                <li><a href="profile.html"><i class="fa fa-user"></i>Update Password</a></li>
		                <li><a href="inbox.html"><i class="fa fa-envelope"></i>Inbox</a></li>
		                <li><a href="calendar.html"><i class="fa fa-calendar"></i>Calender</a></li>
	              	</ul> -->
	            </li>
	           
	        </ul>
	    </div><!-- /.navbar-collapse -->
		<div class="clearfix"></div>
		

		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">
				<ul class="nav" id="side-menu">
				@if(Sentinel::check())

					@if(Sentinel::check()->roles()->first()->slug == 'owner')
					
						<li>
							<a href="{{ url('store/home') }}" class=" hvr-bounce-to-right">
								<i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Dashboard</span> 
							</a>
						</li>
					
						<li>
							<a href="{{ url('store/reports') }}" class=" hvr-bounce-to-right">
								<i class="fa fa-area-chart nav_icon"></i> <span class="nav-label">Reports</span> 
							</a>
						</li>

						<li>
							<a href="{{ url('store/calendar') }}" class=" hvr-bounce-to-right">
								<i class="fa fa-calendar nav_icon"></i> <span class="nav-label">Calendar</span> 
							</a>
						</li>

						<li>
							<a href="{{ url('store/consolidated/orders').'/?date='.date('m/d/Y') }}" class=" hvr-bounce-to-right">
								<i class="fa fa-table nav_icon"></i> <span class="nav-label">Consolidated Orders</span> 
							</a>
						</li>
						
						<li>
							<a href="{{ url('store/customer/orders') }}" class=" hvr-bounce-to-right">
								<i class="fa fa-users nav_icon"></i> <span class="nav-label">Customer Orders</span> 
							</a>
						</li>

						<li>
							<a href="{{ url('store/customer/invoice') }}" class=" hvr-bounce-to-right">
								<i class="fa fa-file nav_icon"></i> <span class="nav-label">Invoice</span> 
							</a>
						</li>
						
						<li>
							<a href="{{ url('store/product/lists') }}" class=" hvr-bounce-to-right">
								<i class="fa fa-leaf nav_icon"></i> <span class="nav-label">Product Lists</span> 
							</a>
						</li>

						<li>
							<a href="{{ url('store/inbox') }}" class=" hvr-bounce-to-right">
								<i class="fa fa-inbox nav_icon"></i> <span class="nav-label">Inbox</span> 
							</a>
						</li>

						<li>
							<a href="{{ url('store/settings') }}" class=" hvr-bounce-to-right">
								<i class="fa fa-cog nav_icon"></i> <span class="nav-label">Settings</span> 
							</a>
						</li>

						<li>
							<a href="#" onClick="document.getElementById('form-logout').submit()" class="hvr-bounce-to-right">
								<i class="fa fa-sign-out nav_icon"></i> <span class="nav-label">Log Out</span>
							</a>
							<form action="/logout" id="form-logout" method="POST">{{ csrf_field() }}</form>
						</li>
					@elseif(Sentinel::check()->roles()->first()->slug == 'customer')
					
						<li>
							<a href="{{ url('customer/home') }}" class=" hvr-bounce-to-right">
							<i class="fa fa-dashboard nav_icon "></i><span class="nav-label">My Orders</span> </a>
						</li>

						<li>
							<a href="{{ url('customer/inbox') }}" class=" hvr-bounce-to-right">
								<i class="fa fa-envelope nav_icon"></i> <span class="nav-label">Inbox</span> 
							</a>
						</li>
										
						<li>
							<a href="{{ url('customer/settings') }}" class=" hvr-bounce-to-right">
								<i class="fa fa-cog nav_icon"></i> <span class="nav-label">Settings</span> 
							</a>
						</li>

						<li>
							<a href="#" onClick="document.getElementById('form-logout').submit()" class="hvr-bounce-to-right">
								<i class="fa fa-sign-out nav_icon"></i> <span class="nav-label">Log Out</span>
							</a>
							<form action="/logout" id="form-logout" method="POST">{{ csrf_field() }}</form>
						</li>
					@endif

				@endif
				</ul>
			</div>
		</div>

		<div id="copy-on-nav" class="sidebar">
			<small> &copy; 2016 Minimal.<br> All Rights Reserved <br> Design by <a href="http://w3layouts.com/" target="_blank" style='color:#CFCFCF'>W3layouts</a> </small>
		</div>
	</div>
</nav>