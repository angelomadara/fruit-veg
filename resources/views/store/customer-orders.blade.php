@extends('master')

@section('title')  @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')
<script>
function isFloat(n) {
    return n === +n && n !== (n|0);
}
function isInteger(n) {
    return n === +n && n === (n|0);
}
$(document)
.ready(function(){
    var credit = $("#g-balance").data('total');
    var total = $("#g-total").data('total');
    if(credit <= 0){
        $("#add-payment").hide();
    }
})
.on('click','#print-page',function(){
    var id = $(this).data('id');
    popupWindow(location.origin+"/store/print/customer/order/"+id, 'Print Guest Information', 1000, 600);
})
.on('click','#add-payment',function(){
    swal({
        title: 'Add Payment',
        html:'Amount<input name="credit" id="swal-input1" class="money-format swal2-input">' +
             'Date<input name="date" id="swal-input2" class="swal2-input">',
        showCancelButton: true,
        confirmButtonText: 'Credit',
        showLoaderOnConfirm: true,
        focusConfirm: true,
        onOpen: () => {
            $(document).find('#swal-input2').blur().datepicker({
                language: 'en',
                autoClose: true,
                position: "bottom left"
            });
        },
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                setTimeout(() => {
                    var credit = $(document).find('#swal-input1').val();
                    var date = $(document).find('#swal-input2').val();
                    var isValid = moment(date, ['MM/DD/YYYY',"YYYY/MM/DD","MM-DD-YYYY","YYYY-MM-DD"],true).isValid();                    
                    if(credit == ""){
                        swal.showValidationError('Credit amount cannot be null')
                    }
                    if (!isValid) {
                        swal.showValidationError('Invalid date')
                    }
                    resolve()
                }, 500)
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    }).then((result) => {
        if(result.value){    
            $.post("/store/credit",{
                credit: $("input[name='credit']").val(),
                date: $("input[name='date']").val(),
                order_id : $("input[name='order_id']").val(),
                _token: $("input[name='_token']").val()
            },function(data){
                if(data.swal.type == 'success'){
                    swal(data.swal).then(function(isOkay){
                        if(isOkay) location.reload(true);
                    });
                }else{
                    swal(data.swal);
                }
            });
        }
    });
})
;
</script>
@endsection

@section('content')

<div class="content-main">
    @csrf
    <input type="hidden" name="order_id" value="{{ $order_id }}">
    <div class="banner">	
        <h2>
			<a href="/store/customer/orders">Customer Orders</a>
			<i class="fa fa-angle-right"></i>
			<span>Orders</span>
		</h2>	   
    </div>

	<div class="content-top">
    <div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>

<div class="tab-pane active text-style" id="tab1">
    <div class="inbox-right">                                
        <div class="mailbox-content">
            <div class="clearfix">
                <div class='float-left'>
                    <h2>
                        {{ ucwords($customer->business_name) }}
                    </h2>
                </div>                                    
                <div class="float-right">
                    @if($order->status == 'pending')
                        <a href="{{ url('store/customer/order/approve').'/'.$order_id }}" class="btn btn-primary btn-sm">
                            Approve
                        </a>
                    @else
                        <h4 style="display:inline-block;">{{ ucwords($order->status) }}</h4>
                    @endif
                    <button id="print-page" data-id="{{$order_id}}" class="btn btn-primary btn-sm"> <i class="fa fa-print"></i> Print</button>
                </div>                                
            </div>
            <hr>
            @if(Session::has('error'))
                <div class='alert alert-danger'> {{ Session::get('error') }} </div>
            @elseif(Session::has('success'))
                <div class='alert alert-success'> {{ Session::get('success') }} </div>
            @endif

            <div class="float-left">
                <div>Order #{{$order_id}} | Date ordered : {{ date("m/d/Y",strtotime($order->created_at)) }} | Date needed : {{ date("m/d/Y",strtotime($order->delivery_date)) }}</div>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th>PRODUCT NAME</th>
                        <th>QUANTITY</th>
                        <th style="text-align:right;">PRICE</th>
                        <th style="text-align:right;">TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                @php 
                    $grandTotal=0;
                    $credit =0; 
                @endphp
                @forelse($items as $product)
                    <tr class="table-row">
                        <td class="table-text">
                            <p>{{ $product->name }}</p>
                        </td>
                        <td class="table-text">
                            <p>{{ $product->need }} {{ $product->measurement }} </p>
                        </td>
                        <td class="table-text" style="text-align:right;">
                            <p>PHP {{ number_format($product->price,2) }}</p>
                        </td>
                        <td class="table-text" style="text-align:right;">
                            <p>PHP {{ number_format($product->need * $product->price,2) }}</p>
                        </td>
                    </tr>
                    @php 
                        $grandTotal += ($product->need * $product->price)
                    @endphp
                @empty
                    <tr class="table-row">
                        <td colspan="4">
                            No orders yet has been made.
                        </td>
                    </tr>
                @endforelse                                    
                </tbody>
                <tfoot>
                    <tr style="text-align:right">
                        <td colspan='3'><h3>Grand total: </h3></td>
                        <td id="g-total" data-total="{{$grandTotal}}">
                            <h3>
                                PHP {{ number_format($grandTotal,2) }}
                            </h3>
                        </td>
                    </tr>
                    <tr style="text-align:right;background:#636b6f;">
                        <td colspan='3'>
                            <h4 style="color:#fff !important;">Payment(s)</h4>
                        </td>
                        <td></td>
                    </tr>
                    
                    @forelse($transactions as $trans)
                        <tr style="text-align:right">
                            <td colspan='3'>{{ date('m/d/Y',strtotime($trans->payment_date)) }}</td>
                            <td> <h4>PHP {{ number_format($trans->credit,2) }}</h4> </td>
                        </tr>
                        @php $credit += $trans->credit; @endphp
                    @empty
                    @endforelse

                    @php
                        $e = $grandTotal-$credit;
                    @endphp

                    <tr style="text-align:right;background:#4CAF50;">
                        <td colspan='3'>
                            @if($e <= 0)
                                <h4 style="color:#fff !important;">Change</h4>
                            @else
                                <h4 style="color:#fff !important;">Balance</h4>
                            @endif
                        </td>
                        <td id="g-balance" data-total="{{($grandTotal-$credit)}}"> <h3 style="color:#fff !important;">PHP {{ number_format($e,2) }}</h3> </td>
                    </tr>

                </tfoot>
            </table>
            <br>
            <div class="float-right">
                @if($e > 0)
                    <button id="add-payment" class="btn btn-primary btn-sm"> <i class="fa fa-money"></i> Add Payment</button>
                @endif
            </div>
            <br>
        </div>
    </div>
</div>
</div>
	</div>

</div>

@endsection