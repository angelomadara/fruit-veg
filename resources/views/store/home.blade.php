@extends('master')

@section('title') Home @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
	<link rel="stylesheet" href="{{ url('css/graph.css') }}">
@endsection

@section('js')
	<script src="{{ url('js/jquery.flot.js') }}"></script>
	<script src="{{ url('js/home.js') }}"></script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<span>Dashboard</span>
			<!-- <i class="fa fa-angle-right"></i> -->
			<!-- <span>Dashboard</span> -->
		</h2>
    </div>

	<div class="content-top">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<div class="box">
				<div class="graph-container">										
					<div id="graph-lines"> </div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="box">
				<div class="col-md-12 top-content">
					<h5>This week orders</h5>					
					<label>{{ $this_week_count }}</label>
					<span style="font-size:.9em;">from {{ $thismonday }} to {{ $thissunday }}</span>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="box">
				<div class="col-md-12 top-content">
					<h5>Next week deliveries</h5>
					<label>{{ $next_week_count }}</label>
					<span style="font-size:.9em;">
						from {{ $nextmonday }} to {{ $nextsunday }}
					</span>
				</div>
				<div class="clearfix"> </div>
			</div>
			
		</div>
	</div>

</div>

@endsection