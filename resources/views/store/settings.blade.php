@extends('master')

@section('title')  @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')
	<script>
		$(document)
			.on('click','#update-password',function(e){
				e.preventDefault();
				var elem = ""+
					"<label>New password</label><input type='password' name='p1' class='swal2-input'>"+
					"<label>Re-type new password</label><input type='password' name='p2' class='swal2-input'>";
				swal({
					title: "Update Password",
					html: elem,
					showCancelButton: true,
					confirmButtonText: 'Update',
					showLoaderOnConfirm: true,
					focusConfirm: true,
					showLoaderOnConfirm: true,
					preConfirm: () => {
						return new Promise((resolve) => {
							setTimeout(() => {
								var p1 = $(document).find("input[name='p1']").val();
								var p2 = $(document).find("input[name='p2']").val();
								if(p1 !== p2){
									swal.showValidationError('Password didn\'t matched')
								}
								if(p1.length <= 7 || p2.length <= 7){
									swal.showValidationError('Password must be 8 characters long')
								}
								resolve()
							}, 500)
						})
					},
					allowOutsideClick: () => !swal.isLoading()
				}).then(function(okay){
					if(okay.value){
						$.post('settings/update-password',{
							p1 : $(document).find("input[name='p1']").val(),
							p2 : $(document).find("input[name='p2']").val(),
							_token : "{{ csrf_token() }}",
						},function(data){
							swal(data.swal).then(function(k){
								if(k.value) location.reload();
							})
						})
					}
				});
			})
		;
	</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<span>Settings</span>
			<!-- <i class="fa fa-angle-right"></i> -->
			<!-- <span>Dashboard</span> -->
		</h2>
    </div>

	<div class="content-top">
		<div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>
			<div class="tab-pane active text-style" id="tab1">
				<div class="inbox-right">                                
					<div class="mailbox-content">
						<div class="clearfix">
							<div class='float-left'>
								<h2>
									Settings
								</h2>
							</div>
						</div>
						<hr>

						<div class="row">
							<div class="col-lg-12">
								<h4><a href="#" id="update-password">Update password</a></h4>
							</div>
						</div>

					</div>
				</div>
			</div>
        </div>
	</div>

</div>

@endsection