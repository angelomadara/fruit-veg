@extends('print-master')


@section('content')
<div class="col-xs-12" id="title">
    <h1>Consolidated Order Report of {{ $date }}</h1>
</div>
    <br><br>
<div class="clearfix"></div>

<div class='col-xs-12'>
    <table class="table">
        <thead>
            <tr>
                <th> Vegitable / Fruit </th>
                <th>  </th>
            </tr>
        </thead>
        <tbody>
            @forelse($orders as $key => $order)
                <tr>
                    <td>{{ ucwords($order['name']) }}</td>
                    <td>{{ $order['sum'] }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan='2'>No orders found</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>

@endsection