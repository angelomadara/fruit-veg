@extends('master')

@section('title')  @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')
<script>
$('.table').DataTable();
$('.print').click(function(e){
	e.preventDefault();
	var orders = $(this).data('orders').split(',').slice(0,-1);
	var id = $(this).data('id');
	popupWindow(location.origin+"/store/customer/invoice/print/"+$(this).data('orders'), 'Print Guest Information', 1000, 600);
})
</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<span>Dashboard</span>
			<!-- <i class="fa fa-angle-right"></i> -->
			<!-- <span>Dashboard</span> -->
		</h2>
    </div>

	<div class="content-top">
		<div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>
			<div class="tab-pane active text-style" id="tab1">
				<div class="inbox-right">                                
					<div class="mailbox-content">
						<div class="clearfix">
							<div class='float-left'>
								<h2>
									INVOICE
								</h2>
							</div>
						</div>
						<hr>

						<div>
							<p><small>List of customers with remaining balance</small></p>
                            <table class="table table-stripe table-bordered table-hover">
								<thead>
									<tr>
										<th> Customer Name </th>
										<th></th>
									</tr>
								</thead>
								<tbody>
								@forelse($customers as $customer)
									<?php 
										$order_nums = "";
										foreach ($customer['orders'] as $order) {
											$order_nums .= $order['order_id'].',';
										}
									?>
									<tr>
										<td> {{ App\Models\Customer::where('customer_id',$customer['customer_id'])->first()->business_name }} </td>
										<td> 
											<a href="#" class="print btn btn-primary btn-xs" data-orders="{{$order_nums}}" data-id="{{$customer['customer_id']}}"> Print</a>
										</td>
									</tr>
								@empty
								@endforelse
								</tbody>
							</table>
                        </div>
					</div>
				</div>
			</div>
        </div>
	</div>

</div>

@endsection