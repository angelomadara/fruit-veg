@extends('master')

@section('title') Reports @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
<link href="{{url('css/font-awesome.css')}}" rel="stylesheet">
@endsection

@section('js')
<script src="{{url('js/Chart.bundle.min.js')}}"></script>
<script>
$.get('reports/monthly',function(sales){

    var ctx = document.getElementById("bar1");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
            datasets: [{
                label: 'Average Paid: PHP ',
                data: sales.paid,
                // backgroundColor: 'rgba(52,152,220, 0.2)',
                borderColor: 'rgba(52,152,220,1)',
                borderWidth: 3
            },{
                label: 'Average Sale: PHP ',
                data: sales.average,
                // backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 3
            }],
        },
        options: {
            responsive: true,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
});
</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<span>Reports</span>
		</h2>
    </div>

	<div class="content-top">
        <!-- content goes here -->
        <div class="graph-grid">
            <div class="col-md-12 graph-1">
                <div class="grid-1">
                    <h4>Montly Sale</h4>
                    <canvas id="bar1" height="300" width="900" style="width: 900; height: 300;"></canvas>
                </div>
            </div>
        </div>
	</div>

</div>

@endsection