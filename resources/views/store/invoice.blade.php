@extends('print-master')


@section('content')

@php $grandTotal = $credit = 0; @endphp

@forelse($orders as $order)
    @php $grandTotal += ($order->need * $order->price) @endphp
@empty 
@endforelse

@forelse($transactions as $trans)
    @php $credit += $trans->credit @endphp
@empty 
@endforelse

<div class="col-xs-12" id="title">
    <h1>INVOICE</h1>
</div>
    <br><br>
<div class="clearfix"></div>


<div class='col-xs-4'>
    <h5>Billed To:</h5>
    <div><b>{{ strtoupper($customer->business_name) }}</b></div>
    <div>Address: {{ ucwords($customer->address) }}</div>
    <div>Telephone: {{ $customer->telephone }} </div>
    <div>Mobile: {{ $customer->mobile }} </div>
</div>

<div class='col-xs-2'>
    <h5><b>Invoice Number</b></h5>
    <div>#{{$order_id}}</div>
    <h5><b>Date Issued</b></h5>
    <div>{{ date('m/d/Y') }}</div>
</div>

<div class="col-xs-6" style="text-align:right;">
    <h1>Total: PHP {{ number_format($grandTotal-$credit,2) }} </h1>
</div>

<div class="clearfix"></div>
    <br>
    <br>
<div class='col-xs-12'>
<table class="table">
    <thead>
        <tr>
            <td><b>PRODUCT NAME</b></td>
            <td><b>QUANTITY</b></td>
            <td><b>PRODUCT PRICE</b></td>
            <td><b>AMOUNT</b></td>
        </tr>
    </thead>
    <tbody>
        @forelse($orders as $order)
            <tr>
                <td>{{ ucwords($order->name) }}</td>
                <td>{{ $order->need }} {{ $order->measurement }} </td>
                <td>PHP {{ number_format($order->price,2) }} </td>
                <td>PHP {{ number_format($order->need * $order->price,2) }}</td>
            </tr>
        @empty style="text-align:right"
        @endforelse
    </tbody>
    <tfoot>
        <tr>
            <td></td>
            <td></td>
            <td style="text-align:right"><b>Grand Total:</b></td>
            <td> <b>PHP {{ number_format($grandTotal,2) }}</b> </td>
        </tr>
        <tr>
            <td colspan='3' style="text-align:right;">
                Payment(s)
            </td>
            <td></td>
        </tr>        
        @forelse($transactions as $trans)
            <tr>
                <td colspan='3' style="text-align:right">{{ date('m/d/Y',strtotime($trans->payment_date)) }} - </td>
                <td> PHP {{ number_format($trans->credit,2) }} </td>
            </tr>
        @empty
        @endforelse
        <tr>
            <td colspan='3' style="text-align:right;">
                <b>Balance</b>
            </td>
            <td> <b>PHP {{ number_format($grandTotal-$credit,2) }}</b></td>
        </tr>
    </tfoot>
</table>
</div>

@endsection