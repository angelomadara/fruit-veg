@extends('master')

@section('title')  @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')
<script>
$(document)
.on('click','#reply-message',function(){
	swal({
		title: "Reply",
		input: "textarea",
		showCancelButton: true,
        confirmButtonText: 'Send',
        showLoaderOnConfirm: true,
        focusConfirm: true,
		showLoaderOnConfirm: true,
		preConfirm: (textarea) => {
            return new Promise((resolve) => {
                setTimeout(() => {
                    if(textarea == ""){
                        swal.showValidationError('You cannot send an empty message')
                    }                    
                    resolve()
                }, 500)
            })
        },
        allowOutsideClick: () => !swal.isLoading()
	}).then(function(okay){
		if(okay.value){
			$.post('reply',{
				message : okay.value,
				_token : "{{ csrf_token() }}",
				id : "{{ $id }}"
			},function(data){
				swal(data.swal).then(function(k){
					if(k.value) location.reload();
				})
			})
		}
	});
})
;
</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
            <a href="{{ url('/store') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
			<a href="{{ url('/store/inbox') }}">Inbox</a>
			<i class="fa fa-angle-right"></i>
			<span>Message #{{ $id }}</span>
		</h2>
    </div>

	<div class="content-top">
		<div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>
			<div class="tab-pane active text-style" id="tab1">
				<div class="inbox-right">                                
					<div class="mailbox-content">
						<div class="clearfix">
							<div class='float-left'>
								<h2>
                                    {{ $tread[(count($tread)-1)]->title }}
								</h2>
							</div>
							<div class="float-right">
								<button id="reply-message" class="btn btn-primary btn-sm">Reply</button>
							</div>
						</div>
						<hr>

						<table class="table table-bordered table-striped table-hover datatable">
                            <tbody>
                            @forelse($tread as $t)
                                <tr class='table-row message-row' data-id="{{ $t->message_id }}">
                                    <td class='table-text'>
                                        <h6> {{ Sentinel::getUser()->id == $t->from ? "Me" : ucwords(App\Models\Customer::where('customer_id',$t->from)->first()->business_name) }} : {{ $t->message }} </h6>
                                        <small> {{ date('M/d/Y h:i A',strtotime($t->created_at)) }} </small>
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>

					</div>
				</div>
			</div>
        </div>
	</div>

</div>

@endsection