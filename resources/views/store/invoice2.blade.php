@extends('print-master')


@section('content')

<div class="col-xs-12" id="title">
    <h1>INVOICE</h1>
</div>
    <br><br>
<div class="clearfix"></div>


<div class='col-xs-4'>
    <h5>Billed To:</h5>
    <div><b>{{ strtoupper($customer[0]->business_name) }}</b></div>
    <div>Address: {{ ucwords($customer[0]->address) }}</div>
    <div>Telephone: {{ $customer[0]->telephone }} </div>
    <div>Mobile: {{ $customer[0]->mobile }} </div>
</div>

<div class='col-xs-2'>
    <!-- <h5><b>Invoice Number</b></h5> -->
    <!-- <div>#</div> -->
    <h5><b>Date Issued</b></h5>
    <div>{{ date('m/d/Y') }}</div>
</div>

<div class="col-xs-6" style="text-align:right;">
</div>

<div class="clearfix"></div>
    <br>
    <br>
<div class='col-xs-12'>

@forelse($customer as $cus)
@php 
    $grandTotal = $credit = 0; 
@endphp
<div class='col-xs-12'>
    <h5><b>Order Number #{{ $cus->order_id }}</b></h5>
</div>
<table class="table">
    <thead>
        <tr>
            <td><b>PRODUCT NAME</b></td>
            <td><b>QUANTITY</b></td>
            <td><b>PRODUCT PRICE</b></td>
            <td><b>AMOUNT</b></td>
        </tr>
    </thead>
    <tbody>
    <?php
        $orders = App\Models\Orders::where('order_id',$cus->order_id)->get();
    ?>
    @forelse($orders as $order)
        <tr>
            <td> {{ App\Models\Products::where('id',$order->product_id)->first()->name }} </td>
            <td> {{ $order->need }} </td>
            <td> {{ $order->price }} </td>
            <td> {{ $order->need * $order->price }} </td>
        </tr>
        @php
            $grandTotal+= $order->need * $order->price;
            $credit = DB::select("SELECT SUM(credit) AS x FROM transactions WHERE order_id = ?",[$cus->order_id])[0]->x;
        @endphp
        
    @empty
    @endforelse
    </tbody>
    <tfoot>
        <tr>
            <td></td>
            <td></td>
            <td style="text-align:right"><b>Grand Total:</b></td>
            <td> <b>PHP {{ number_format($grandTotal,2) }}</b> </td>
        </tr>
        <tr>
            <td colspan='3' style="text-align:right;">
                Total Paid
            </td>
            <td>PHP {{ number_format($credit,2) }}</td>
        </tr>        
        
        <tr>
            <td colspan='3' style="text-align:right;">
                <b>Balance</b>
            </td>
            <td> <b>PHP {{ number_format($grandTotal-$credit,2) }}</b></td>
        </tr>
    </tfoot>
</table>
@empty
@endforelse

</div>

@endsection