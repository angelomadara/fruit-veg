@extends('master')

@section('title') Inbox @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')
<script>
$(document)
.ready(function(){
    $('.datatable').DataTable({
        "order": [[ 0, "desc" ]]
    });
})
.on('click','.print-invoice',function(){
    var id = $(this).data('id');
    // {order}
    popupWindow(location.origin+"/store/print/customer/order/"+id, 'Print Guest Information', 1000, 600);
})
.on('click','.delivered',function(){
    var id = $(this).data('id');
    swal({
        "title" : "Products has been delivered?",
        showCancelButton: true,
        confirmButtonText: 'Yes',
        showLoaderOnConfirm: true,
        focusConfirm: true,
    }).then((result)=>{
        if(result.value){
            $.get("/store/delivered",{"id":id},function(data){
                swal(data.swal).then((result)=>{
                    if(result.value) location.reload();
                });
            })
        }
    });
})
;
</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<span>Customer Orders</span>
		</h2>
    </div>
	<div class="content-top">
        <!-- content goes here -->
        <div class="inbox-mail">
        <!-- tab content -->
            <div class="col-md-12">
                <div class="tab-pane active text-style" id="tab1">
                    <div class="inbox-right">                                
                        <div class="mailbox-content">
            
                            <div class="mail-toolbar clearfix">
                                <div class='float-left'>
                                    <h2>
                                        Customer Orders
                                    </h2>
                                </div>                               

                                <div class="clearfix"></div>
                                <!-- <hr> -->
                                <br>
                                <table class="table table-bordered table-striped table-hover datatable">
                                    <thead>
                                        <tr class="active">
                                            <th>ORDER ID</th>
                                            <th>COMPANY NAME</th>
                                            <th>DELIVERY DATE</th>
                                            <th>ORDERED DATE</th>
                                            <th>STATUS</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($orders as $order)
                                            @php 
                                                $os = App\Models\Orders::where('order_id',$order->order_id)->get();
                                                $debit = $credit = 0;
                                                foreach($os as $o){ $debit+=($o->price*$o->need); }
                                                $trans = App\Models\Transactions::where('order_id',$order->order_id)->get();
                                                foreach($trans as $tran){ $credit+=$tran->credit; }
                                            @endphp
                                            <tr>
                                                <td>
                                                    <a href="{{ url('store/customer/order').'/'.$order->customer_id.'/'.$order->order_id }}">
                                                        <h5>#{{ $order->order_id }} </h5>
                                                    </a>
                                                </td>
                                                <td>
                                                    <h5>{{ strtoupper($order->business_name) }}</h5>
                                                </td>
                                                <td>
                                                    <h5>{{ date("M d, Y",strtotime($order->delivery_date)) }}</h5>
                                                </td>
                                                <td>
                                                    <h5>{{ date('M d, Y',strtotime($order->created_at)) }}</h5>
                                                </td>
                                                <td>
                                                    <h5>
                                                        {{ $order->status == "completed" ? "Delivered" : ucwords($order->status) }}  
                                                        @php
                                                            if($order->status == "confirmed" || $order->status == "completed"){
                                                                echo (($debit - $credit) < 1 ? "<label style=\"color:green;font-size:.9em;\">(Paid)</label>" : "<label style=\"color:orange;font-size:.9em;\">(with balance)</label>");
                                                            }
                                                        @endphp
                                                        
                                                        <!-- echo "($debit - $credit) < 1 ? '' : ''"; -->
                                                    </h5>
                                                </td>
                                                <td>
                                                    <a href="{{ url('store/customer/order').'/'.$order->customer_id.'/'.$order->order_id }}" class="btn btn-primary btn-xs">
                                                        View
                                                    </a>
                                                    @if($order->status == "confirmed" || $order->status == "completed")
                                                        <button data-id="{{ $order->order_id }}" class="print-invoice btn btn-primary btn-xs">
                                                            Print
                                                        </button>
                                                    @endif
                                                    @if($order->status != "delivered" && $order->status == "confirmed")
                                                        <button data-id="{{ $order->order_id }}" class="delivered btn btn-primary btn-xs">
                                                            Delivered
                                                        </button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="5">
                                                    <h4>No customer orders found</h4>
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
	</div>

</div>

@endsection