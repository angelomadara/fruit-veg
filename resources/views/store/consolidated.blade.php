@extends('master')

@section('title')  @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')
<script>
$(document)
.on('click','a.change-date',function(e){
    e.preventDefault();
    swal({
        title: 'Date of delivery<br> Ex. 01/23/2018',
        input: 'text',
        showCancelButton: true,
        confirmButtonText: 'Change',
        focusConfirm: true,
        onOpen: () => {
            $(document).find('.swal2-input').blur().datepicker({
                language: 'en',
                autoClose: true,
            }).css({'text-align':'center'});
        },
        showLoaderOnConfirm: true,preConfirm: (text) => {
            return new Promise((resolve) => {
                setTimeout(() => {
                    var date = text;
                    var isValid = moment(date, ['MM/DD/YYYY',"YYYY/MM/DD","MM-DD-YYYY","YYYY-MM-DD"],true).isValid();                    
                    if (!isValid) {
                        swal.showValidationError('Invalid date')
                    }
                    resolve()
                }, 500)
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    }).then((result) => {
        var date = result.value;
        var isValid = moment(date, ['MM/DD/YYYY',"YYYY/MM/DD","MM-DD-YYYY","YYYY-MM-DD"],true).isValid();
        // alert('okay'); return;
        if(isValid){
            location.href = location.origin+"/store/consolidated/orders?date="+date;
        }
        return;
    });
})
.on('click','#print-orders',function(e){
    e.preventDefault();
    var date = $("input[name='date']").val();
    popupWindow(location.origin+"/store/print/consolidated/order?date="+date, 'Consolidated orders for '+date, 1000, 600);
})
;
</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<span>Consolidated</span>
			<!-- <i class="fa fa-angle-right"></i> -->
			<!-- <span>Dashboard</span> -->
		</h2>
    </div>

	<div class="content-top">
		<div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>
			<div class="tab-pane active text-style" id="tab1">
				<div class="inbox-right">                                
					<div class="mailbox-content">
						<div class="clearfix">
							<div class='float-left'>
								<h2>
									Consolidated Orders for - {{ date('M d, Y',strtotime($date)) }}
                                    <input type="hidden" name="date" value="{{ date('M d, Y',strtotime($date)) }}">
								</h2>
							</div>
                            <div class='float-right'>
                                <a href="#" class="change-date btn btn-default">Change Date</a>
                                <a href="" class="btn btn-primary" id="print-orders">Print</a>
                            </div>
						</div>
						<hr>

                        <table class="table">
                            <thead>
                                <tr>
                                    <td> Vegitable / Fruit </td>
                                    <td>  </td>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($combine as $key => $prod)
                                    <tr>
                                        <td>{{ ucwords($prod['name']) }}</td>
                                        <td>{{ $prod['sum'] }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan='2'>No orders found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
						                        
					</div>
				</div>
			</div>
        </div>
	</div>

</div>

@endsection