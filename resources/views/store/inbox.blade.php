@extends('master')

@section('title') Inbox @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
<style>
.table-row{
    cursor: pointer;
}
.table-row:hover{
    text-decoration:underline;
}
</style>
@endsection

@section('js')
<script>
$('.datatable').DataTable();
$(document)
    .on('click','.delete-msg',function(e){
        var id = $(this).closest("tr").data('id');
        swal({
            title: 'Delete message?',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            showLoaderOnConfirm: true,
            focusConfirm: true,
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {
            if(result.value){
                $.get("inbox/remove/"+id,function(data){
                    if(data.swal.type == 'success'){
                        swal(data.swal).then(function(isOkay){
                            if(isOkay) location.reload(true);
                        });
                    }else{
                        swal(data.swal);
                    }
                });
            }
        });
    })
    .on('click','tbody > tr > td:not(:first-child)',function(){
        var id = $(this).closest("tr").data('id');
        location.href = "inbox/"+id;
    })
    .on('click','#announcement',function(){
        swal({
		title: "Announcement",
        text: "",
		html: "This message will be send to all customer"+
                "<input name='title' class='swal2-input' placeholder='title'>"+
                "<textarea name='message' placeholder='message' style='min-height:200px;' rows='10' cols='10' class='swal2-input'></textarea>",
		showCancelButton: true,
        confirmButtonText: 'Send',
        showLoaderOnConfirm: true,
        focusConfirm: true,
		showLoaderOnConfirm: true,
		preConfirm: (textarea) => {
            return new Promise((resolve) => {
                setTimeout(() => {
                    var input = $(document).find("input[name='title']");
                    var textarea = $(document).find("textarea[name='message']");
                    if(input.val() == "" || textarea.val() == ""){
                        swal.showValidationError('You cannot send an empty message')
                    }                    
                    resolve()
                }, 500)
            })
        },
        allowOutsideClick: () => !swal.isLoading()
	}).then(function(okay){
		if(okay.value){
			$.post('/store/inbox/announcement',{
				message : okay.value,
				_token : "{{ csrf_token() }}",
			},function(data){
				swal(data.swal).then(function(k){
					if(k.value) location.reload();
				})
			})
		}
	});
    })
;
</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<span>Inbox</span>
		</h2>
    </div>
    <br>
    <!-- tab content -->
    <div class="col-md-12">
        <div class="tab-pane active text-style" id="tab1">
            <div class="inbox-right">       
                <div class="mailbox-content">
                    <div class="float-right">
                        <button id="announcement" class='btn btn-primary btn-sm'>Send Announcement</button>
                    </div>
                    <br>
                    <table class="table table-bordered table-striped table-hover datatable">
                        <thead>
                            <tr>
                                <th style="width:5%">&nbsp;</th>
                                <th>COMPANY</th>
                                <th>MESSAGE</th>
                                <th>DATE SENT</th>
                                <th>STATUS</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($inbox as $box)
                                <tr class="table-row" data-id="{{ $box->message_id }}">
                                    <td>
                                        <button class='delete-msg btn btn-default btn-sm'>
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                    <td class="table-title">
                                        {{ ucwords(strtolower($box->business_name)) }}
                                    </td>
                                    <td class="table-text">
                                        <h6> {{ $box->title }} </h6>
                                        <!-- <p> {{ mb_strimwidth($box->message,0,100,'...') }} </p> -->
                                        @php
                                            echo mb_strimwidth(App\Models\Inbox::where('message_id',$box->message_id)->orderBy('created_at','desc')->first()->message,0,100,'...');
                                        @endphp
                                    </td>
                                    <td class="march">
                                        {{ date('M/d/Y',strtotime($box->created_at)) }} 
                                    </td>
                                    <td >
                                        <!-- {{ ucwords($box->status) }} -->
                                        @php
                                            echo App\Models\Inbox::where('message_id',$box->message_id)->orderBy('created_at','desc')->first()->status;
                                        @endphp
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
    <div class="clearfix"> </div>
</div>

@endsection