@extends('master')

@section('title') Inbox @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')
<script>
$('.datatable').DataTable({
    "order": [[ 1, "asc" ]]
});
$(document)
.on('click','a.remove-product',function(e){
    e.preventDefault();
    var id = $(this).data('id');
    var thisUrl = $(this).attr('href');
    
    swal({
        title: 'Do you really want to remove this product?',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        showLoaderOnConfirm: true,
        focusConfirm: true,
        allowOutsideClick: () => !swal.isLoading()
    }).then((result) => {
        if(result.value){
            
            $.get(thisUrl,function(data){
                if(data.swal.type == 'success'){
                    swal(data.swal).then(function(isOkay){
                        if(isOkay) location.reload(true);
                    });
                }else{
                    swal(data.swal);
                }
            });
        }
    });
})
.on('click','a.update-quantity',function(e){
    e.preventDefault();
    var id = $(this).data('id');
    swal({
        title: 'Add Quantity',
        input: 'number',
        showCancelButton: true,
        confirmButtonText: 'Update',
        showLoaderOnConfirm: true,
        focusConfirm: true,
        onOpen: () => { },
        showLoaderOnConfirm: true,
        preConfirm: (number) => {
            return new Promise((resolve) => {
                setTimeout(() => {
                    var text = number;
                    if(text == "" || text == undefined){
                        swal.showValidationError('Quantity cannot be empty')
                    }
                    if(quantity <= 0 && text < 0) swal.showValidationError('Cannot accept negative number now')
                    // if(text < 0) swal.showValidationError('Cannot accept negative number')
                    resolve()
                }, 500)
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    }).then((result) => {
        if(result.value){    
            $.post("/store/product/quantity",{
                quantity : result.value,
                id : id,
                _token: "{{ csrf_token() }}"
            },function(data){
                if(data.swal.type == 'success'){
                    swal(data.swal).then(function(isOkay){
                        if(isOkay) location.reload(true);
                    });
                }else{
                    swal(data.swal);
                }
            });
        }
    });
})
;
</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<span>Products</span>
		</h2>
    </div>

	<div class="content-top">
        <!-- content goes here -->
        <div class="inbox-mail">
        <!-- tab content -->
            <div class="col-md-12">
                <div class="tab-pane active text-style" id="tab1">
                    <div class="inbox-right">                                
                        <div class="mailbox-content">
            
                            <div class="mail-toolbar clearfix">
                                <div class='float-left'>
                                    <h2>
                                        Product Inventory
                                    </h2>
                                </div>                                    
                                <div class="float-right">

                                    <a href="{{ url('store/product-form') }}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-pencil-square-o icon_9"></i>
                                        Add Product
                                    </a>
                                    
                                </div>                                
                            </div>

                            <table class="table table-bordered table-striped datatable">
                                <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>Products</th>
                                        <th>Quantity</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($products as $product)
                                    <tr class="table-row">
                                        <td class="table-img" style="width:20%;">
                                            <img src="{{ url('images/small/').'/'.$product->photo }}" style="width:100%;" alt="" />
                                        </td>
                                        <td class="table-text" style="width:30%;" >
                                            <h6>{{ $product->name }}</h6>
                                            <p>{{ $product->note }}</p> 
                                            <h6>PHP {{ number_format($product->price,2) }}</h6> 
                                        </td>
                                        <td style="width:10%;">
                                            {{ $product->quantity }} {{ App\Models\Per::where('id',$product->per)->first()->code }}
                                        </td>
                                        <td class="setting" style="width:15%">
                                            <a href="{{ url('store/product-form?action=update&id='.$product->id) }}">
                                                <div> <i class="fa fa-pencil"></i> Update Product </div>
                                            </a>
                                            <a href="" class="update-quantity" data-id="{{$product->id}}">
                                                <div> <i class="fa fa-pencil"></i> Update Quantity </div>
                                            </a>
                                            <a href="{{ url('store/product-remove/'.$product->id) }}" class="remove-product" data-id="{{$product->id}}">
                                                <div> <i class="fa fa-eye"></i> Remove Product </div>
                                            </a>
                                        </td>   
                                    </tr>
                                @empty
                                    <tr class="table-row">
                                        <td colspan="3">
                                            No products found
                                        </td>
                                    </tr>
                                @endforelse                                    
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
	</div>

</div>

@endsection