@extends('master')

@section('title') Calendar @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
<!-- <link rel="stylesheet" type="text/css" href="{{url('css/calendar.css')}}" /> -->
		<!-- <link rel="stylesheet" type="text/css" href="{{url('css/custom_1.css')}}" /> -->
		<link rel="stylesheet" href="{{ url('bower/fullcalendar/dist/fullcalendar.min.css') }}">
		<!-- <link rel="stylesheet" href="{{ url('bower/fullcalendar/dist/fullcalendar.print.min.css') }}"> -->
@endsection

@section('js')
<!-- <script type="text/javascript" src="{{url('js/jquery.calendario.js')}}"></script> -->
<script src="{{ url('bower/fullcalendar/dist/fullcalendar.min.js') }}"></script>
<!-- <script type="text/javascript" src="{{url('js/data.js')}}"></script> -->
<script type="text/javascript">	
	// $(function() {	
	
		$("#calendar").fullCalendar({
			defaultView: 'month',
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			navLinks: true, 
			eventLimit: true,
			events : {
				url: 'calendar/orders',
				type: 'get',
			},
			eventRender: function(event, element) {
				element.find('.fc-title').html(event.title);
				// element.qtip({
				// 	content: event.description
				// });
			}			
		});
	
</script>
@endsection

@section('content')


<div class="content-main">

    <div class="banner">		   
		<h2>
			<span>Calendar</span>
		</h2>
    </div>

	<div class="content-top">
        <!-- content goes here -->
        <div class="calendar">
			<div class="clearfix" style="background:#fff;padding:1em;">
					<!-- <div class="custom-header">

						<h3 class="custom-month-year">
							<span id="custom-month" class="custom-month"> </span>
							<span id="custom-year" class="custom-year"> </span>
							<nav>
								<span id="custom-prev" class="custom-prev"> </span>
								<span id="custom-next" class="custom-next"> </span>
								<span id="custom-current" class="custom-current" title="Got to current date"></span>
							</nav>
							<div class="clearfix"> </div>
						</h3>
					</div> -->
					
					<!-- <div class="clearfix"> </div> -->
				
				<div class="">
					<div class="col-lg-12">
					<div id="calendar"></div>
					</div>
				</div>
			</div>
	 	</div>
		 
	</div>

</div>

@endsection