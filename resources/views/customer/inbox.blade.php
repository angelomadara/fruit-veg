@extends('master')

@section('title')  @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')
<script>
$(document)
.ready(function(){
    $('.datatable').DataTable();
})
.on('click','.remove-msg',function(e){
    var id = $(this).closest('tr').data('id');
    swal({
        title: 'Delete message?',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        showLoaderOnConfirm: true,
        focusConfirm: true,
        allowOutsideClick: () => !swal.isLoading()
    }).then((result) => {
        if(result.value){
            $.get("inbox/remove/"+id,function(data){
                if(data.swal.type == 'success'){
                    swal(data.swal).then(function(isOkay){
                        if(isOkay) location.reload(true);
                    });
                }else{
                    swal(data.swal);
                }
            });
        }
    });
})
.on('click','#send-message',function(e){
    e.preventDefault();
    swal({
        title: 'Send message',
        html: "<div style='text-align:left;'>Title:</div><input type='text' class='swal2-input' id='title' name='title'><div style='text-align:left;'>Message:</div><textarea class='swal2-input' style='height:200px;' id='message'></textarea>",
        showCancelButton: true,
        confirmButtonText: 'Submit',
        showLoaderOnConfirm: true,
        focusConfirm: true,
        onOpen: () => {
            
        },
        showLoaderOnConfirm: true,preConfirm: () => {
            return new Promise((resolve) => {
                setTimeout(() => {
                    var title = $("input#title").val();
                    var message = $("textarea#message").val();
                    if(message == "" || title == ""){
                        swal.showValidationError('You cannot send an empty message')
                    }                    
                    resolve()
                }, 500)
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    }).then((result) => {
        if(result.value){
            $.post("inbox/create",{
                'title' : $("input#title").val(),
                'message' : $("textarea#message").val(),
                '_token' : "{{ csrf_token() }}"
            },function(data){
                swal(data.swal).then(function(asdf){
                    if(asdf.value) location.reload()
                });
            });
        }
    });
})
.on('click','tr td:not(:first-child)',function(){
    var id = $(this).closest('tr').data('id');
    location.href = "/customer/inbox/"+id;
})
;
</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
            <a href="{{ url('/customer') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
			<span>Inbox</span>
		</h2>
    </div>

	<div class="content-top">
		<div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>
			<div class="tab-pane active text-style" id="tab1">
				<div class="inbox-right">                                
					<div class="mailbox-content">
						<div class="clearfix">
							<div class='float-left'>
								<h2>
									INBOX
								</h2>
							</div>  
                            <div class="float-right">
                                <button class="btn btn-primary" id="send-message">
                                    Send New Message
                                </button>
                            </div>
						</div>
						<hr>

						<table class="table table-bordered table-striped table-hover datatable">
                            <thead>
                                <tr>
                                    <td style="width:5%;"> &nbsp; </td>
                                    <!-- <td style="width:15%;"> MESSAGE ID </td> -->
                                    <td style="width:20%;">TITLE</td>
                                    <td style="width:30px;">MESSAGE</td>
                                    <td style="width:5%;"> &nbsp; </td>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($messages as $message)
                                    <tr class='message-row' data-id="{{ $message->message_id }}" style="cursor:pointer;">
                                        <td style="width:20px;"> <button class="remove-msg btn btn-default btn-sm"><i class="fa fa-trash"></i></button> </td>
                                        <!-- <td> {{ $message->message_id }} </td> -->
                                        <td> {{ $message->title }} </td>
                                        <td> {{ mb_strimwidth($message->message,0,100,'...') }} </td>
                                        <td> {{ $message->status }} </td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
        </div>
	</div>

</div>

@endsection