@extends('master')

@section('title') Order Form @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
	<style>
		#table-cart tbody tr td:last-child,
		#table-cart tfoot tr td:last-child{
			text-align: right;
		}
	</style>
@endsection

@section('js')
<script src="{{ url('js/functions.js') }}"></script>
<script src="{{ url('js/order-form.js') }}"></script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<a href="home">Home</a>
			<i class="fa fa-angle-right"></i>
			<span>Order Form</span>
		</h2>
    </div>

	<div class="content-top">
		<div class='col-lg-8 col-md-8 col-sm-6 col-xs-6'>

		<div class="tab-pane active text-style" id="tab1">
			<div class="inbox-right">                                
				<div class="mailbox-content">
					<div class="clearfix">
						<div class='float-left'>
							<h2>
								<i class="fa fa-utensils"></i> Products
							</h2>
						</div>                                    
						<!-- <div class="float-right">
                            <div class="float-right">
                                <input type="text" class="form-control" placeholder="Search...">
                            </div>							
						</div>                                 -->
					</div>
					<hr>
					<table class="table datatable">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Name</th>
								<th>&nbsp;</th>
							</tr>	
						</thead>
						<tbody>
						@forelse($products as $product)
							<tr class="table-row">
								<td class="table-img" style="width:30%;">
									<img src="{{ url('images/small/').'/'.$product->photo }}" style="width:100%;" alt="" />
								</td>
								<td class="table-text">
									<h6>{{ $product->name }}</h6>
									<p>PHP <span class="presyo" style="color:#444;">{{ number_format($product->price,2) }}</span> / {{ $product->per }} </p> 
									<p>Category : {{ $product->category }}</p>
								</td>                                 
								<!-- <td >
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>                       
								</td> -->
								<td class="setting" style="width:15%">
									<div class="btn-group" role="group" data-per="{{$product->percode}}" data-id="{{$product->id}}" aria-label="...">
										<button type="button" data-operator="sub" class="sub-to-cart btn btn-default">-</button>
										<button type="button" data-operator="add" class="add-to-cart btn btn-default">+</button>
									</div>
									<!-- <a class="add-to-cart" href="{{ url('customer/add-product/'.$product->id) }}">
										<div> <i class="fa fa-plus"></i> Add </div>
									</a> -->
								</td>   
							</tr>
						@empty
							<tr class="table-row">
								<td colspan="3">
									
								</td>
							</tr>
						@endforelse                                    
						</tbody>
						<tfoot>
							<tr>
								<td colspan='4'>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		</div>

		<div class='col-lg-4 col-md-6 col-sm-6 col-xs-6'>
			<div class='inbox-right'>
				<div class='mailbox-content'>
					<div class="clearfix">
						<div class="float-left"><h2> <i class="fa fa-shopping-cart "></i> Cart</h2></div>
						<div class="float-right">
							<button class="btn btn-success" id="send-order">Send Order</button>
							@csrf
							<input type="hidden" value="" name="order_id">
						</div>
					</div>
					<hr>
					<div class="clearfix">
						<table id="table-cart" class="table">
							<tbody></tbody>
							<tfoot>
								<tr>
									<td><b>TOTAL:</b></td>
									<td><b>PHP 0.00</b></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

@endsection