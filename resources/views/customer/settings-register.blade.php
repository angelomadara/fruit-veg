@extends('master')

@section('title')  @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<span>Settings</span>
			<!-- <i class="fa fa-angle-right"></i> -->
			<!-- <span>Dashboard</span> -->
		</h2>
    </div>

	<div class="content-top">
		<div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>
        <div class="grid-form1 clearfix">
            <h3 id="forms-example" class="">Profile Information</h3>
                @if(Session::has('error'))
                    <div class='alert alert-danger'> {{ Session::get('error') }} </div>
                @elseif(Session::has('success'))
                    <div class='alert alert-success'> {{ Session::get('success') }} </div>
                @endif
            <form class="col-lg-5" method="POST" action="update">
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Company Name *</label>
                    <input type="text" value="{{ $customer ? $customer->business_name : "" }}" name="company" class="form-control" id="exampleInputEmail1" placeholder="Company name">
                </div>
                
                <div class="form-group">
                    <label for="exampleInputPassword1">Address *</label>
                    <input type="text" value="{{ $customer ? $customer->address : "" }}" name="address" class="form-control" id="exampleInputPassword1" placeholder="Company address">
                </div>
                
                <div class="form-group">
                    <label for="exampleInputPassword1">Telephone Number *</label>
                    <input type="text" value="{{ $customer ? $customer->telephone : "" }}" name="telephone" class="form-control" id="exampleInputPassword1" placeholder="Telephone">
                </div>
                
                <div class="form-group">
                    <label for="exampleInputPassword1">Mobile Number *</label>
                    <input type="text" value="{{ $customer ? $customer->mobile : "" }}" name="mobile" class="form-control" id="exampleInputPassword1" placeholder="Mobile">
                </div>
                
                <div class="form-group">
                    <label for="exampleInputPassword1">Contact Person *</label>
                    <input type="text" value="{{ $customer ? $customer->contact_person : "" }}" name="contact" class="form-control" id="exampleInputPassword1" placeholder="Contact person">
                </div>

                <div class="checkbox">
                    <label>
                        <input name="agree" type="checkbox"> I Agree that all information above are correct and meet the terms and conditions.
                    </label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        </div>
	</div>

</div>

@endsection