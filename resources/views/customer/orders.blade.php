@extends('master')

@section('title')  @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')
<script>

</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
        <h2>
			<a href="/customer/home">Home</a>
			<i class="fa fa-angle-right"></i>
			<span>Order</span>
		</h2>
    </div>

	<div class="content-top">
        <div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>
        <div class="tab-pane active text-style" id="tab1">
			<div class="inbox-right">                                
				<div class="mailbox-content">
                    
                    <div class="clearfix">
						<div class='float-left'>
							<h2>
								<i class="fa fa-utensils"></i> Order #{{$id}}
							</h2>
						</div>
                        <div class='float-right'>
                            {{ ucwords($order->status) }}
                        </div>
					</div>

                    <div class="clearfix">
                        <div class="float-left">
                            &nbsp;&nbsp;&nbsp;Date Needed : {{ date('m/d/Y',strtotime($order->delivery_date)) }}
                            <br>
                            &nbsp;&nbsp;&nbsp;Date Ordered : {{ date('m/d/Y',strtotime($order->created_at )) }}
                        </div>
                    </div>
                    
					<hr>

                    <div class="clearfix">
                        <div class="float-right">
                            {!! $order->status == "pending" ? "<a href='".url('/customer/order/'.$id.'/update')."' class='btn btn-primary btn-sm'>Update Order</a>" : "" !!}
                        </div>
                    </div>

                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Product Name</th>
                                <th style="text-align:right">Selling Price</th>
                                <th style="text-align:right">Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php 
                                $grand_total = 0;
                            @endphp
                            @forelse($items as $item)
                                <tr>
                                    <td> {{ $item->need }} {{ $item->measurement }} </td>
                                    <td> {{ $item->product }} </td>
                                    <td style="text-align:right"> PHP {{ number_format($item->price,2) }} </td>
                                    <td style="text-align:right"> PHP {{ number_format($item->price * $item->need,2) }} </td>
                                </tr>
                                @php $grand_total += ($item->price * $item->need) @endphp
                            @empty
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan='2'>&nbsp;</td>
                                <td style="text-align:right">
                                    <h3>
                                        TOTAL:
                                    </h3>
                                </td>
                                <td style="text-align:right">
                                    <h3>
                                        PHP {{ number_format($grand_total,2) }}
                                    </h3>
                                </td>
                            </tr>

                            <tr style="text-align:right;background:#636b6f;">
                                <td colspan='3'>
                                    <h4 style="color:#fff !important;">Payment(s)</h4>
                                </td>
                                <td></td>
                            </tr>
                            <?php $credit=0; ?>
                            @forelse($trans as $tran)
                                <tr style="text-align:right">
                                    <td colspan='3'>{{ date('m/d/Y',strtotime($tran->payment_date)) }}</td>
                                    <td> <h4>PHP {{ number_format($tran->credit,2) }}</h4> </td>
                                </tr>
                                @php $credit += $tran->credit; @endphp
                            @empty
                            @endforelse

                            <tr style="text-align:right;background:#4CAF50;">
                                <td colspan='3'>
                                    <h4 style="color:#fff !important;">Balance</h4>
                                </td>
                                <td> <h3 style="color:#fff !important;">PHP {{ number_format($grand_total-$credit,2) }}</h3> </td>
                            </tr>

                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        </div>
	</div>

</div>

@endsection