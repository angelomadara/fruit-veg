@extends('master')

@section('title')  @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')
<script>
$(document)
.on('click','#a-update-password',function(e){
	e.preventDefault();
	var elements = ""+
	"<div>"+
		"<input type='password' id='password1' class='swal2-input' placeholder='new password'>"+
		"<input type='password' id='password2' class='swal2-input' placeholder='confirm new password'>"+
	"</div>";
	swal({
        title: 'Update password',
        html: elements,
        showCancelButton: true,
        confirmButtonText: 'Change',
        focusConfirm: true,
        onOpen: () => {
        },
        showLoaderOnConfirm: true,preConfirm: (text) => {
            return new Promise((resolve) => {
                setTimeout(() => {
					var password1 = $(document).find("#password1").val();
					var password2 = $(document).find("#password2").val();

					if(password1 == "" || password2 == "") swal.showValidationError("Passwords cannot be empty")
					if (password1 != password2) swal.showValidationError('Passwords didn\'t matched')
					if ((password1.length < 8)  ||  (password2.length < 8)) swal.showValidationError('Passwords must be 8 characters long')
                    resolve()
                }, 500)
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    }).then((result) => {
        var password = result.value;
        if(result.value){
			$.post('settings/update/password',{
				'password1' : $(document).find("#password1").val(),
				'password2' : $(document).find("#password2").val(),
				'_token' 	: "{{ csrf_token() }}"
			},function(data){
				swal(data.swal);
			});
		}
    });
})
;
</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<span>Settings</span>
			<!-- <i class="fa fa-angle-right"></i> -->
			<!-- <span>Dashboard</span> -->
		</h2>
    </div>

	<div class="content-top">
		<div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>
		<div class="tab-pane active text-style" id="tab1">
			<div class="inbox-right">                                
				<div class="mailbox-content">
					<div class="clearfix">
						<h3 id="forms-example" class="float-left">Profile Information</h3>
						<div class='float-right'>
							<a href="{{ url('customer/settings/update') }}">Update</a>
						</div>
					</div>
					<hr>
					<table class="table">
						<tbody>
							<tr>
								<td>Business Name:</td>
								<td>{{ $customer ? strtoupper($customer->business_name) : ""}}</td>
							</tr>
							<tr>
								<td>Business Address:</td>
								<td>{{ $customer ? ucwords($customer->address) : ""}}</td>
							</tr>
							<tr>
								<td>Business Telephone:</td>
								<td>{{ $customer ? strtoupper($customer->telephone) : ""}}</td>
							</tr>
							<tr>
								<td>Business Mobile:</td>
								<td>{{ $customer ? strtoupper($customer->mobile) : ""}}</td>
							</tr>
							<tr>
								<td>Business Contact Person:</td>
								<td>{{ $customer ? ucwords($customer->contact_person) : ""}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
        <div class="grid-form1 clearfix">
			<a id="a-update-password" href="{{ url('customer/settings/update/password') }}">Update password</a>
        </div>
        </div>
	</div>

</div>

@endsection