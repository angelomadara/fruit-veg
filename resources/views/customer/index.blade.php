@extends('master')

@section('title') Order Form @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')
<script>
$('.datatable').DataTable({
	order: [[ 0 , 'desc' ]]
});
$(document)
.on('click','.cancel-btn',function(e){
	var id = $(this).data('id');
	swal({
		title: 'Are you sure you want to cancel this order?',
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, cancel it!'
	}).then((result) => {
		if (result.value) {
			$.post('/customer/cancel-order',{'id':id,"_token":$("input[name='_token']").val()},function(data){
				swal(data.swal).then(function(isOkay){
					if(isOkay) location.reload(true);
				});
			});
		}
	})
})
.on('click','#to-order-form',function(e){
	e.preventDefault();
	var url = $(this).attr('href');
	$.get('/customer/profile/check',function(data){
		if(data.isValid == true){
			location.href = url;
		}else{
			swal({
				'title' : "Fill up first your company profile before you can make an order",
				'type' : 'info'
			}).then(function(){
				location.href = "/customer/settings/update";
			})
		}
	});
})
;
</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<span>Home</span>
		</h2>
    </div>

	<div class="content-top">
		<div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>

		<div class="tab-pane active text-style" id="tab1">
			<div class="inbox-right">                                
				<div class="mailbox-content">
					<div class="clearfix">
						<div class='float-left'>
							<h2>
								My Orders
							</h2>
						</div>                                    
						<div class="float-right">
							<a href="{{ url('customer/order-form') }}" id="to-order-form" class="btn btn-primary btn-sm" title="">
								<i class="fa fa-pencil-square-o icon_9"></i>
								Add Order
							</a>
						</div>                                
					</div>
					@csrf
					<table class="table table-bordered table-striped table-hover datatable">
						<thead>
							<tr>
								<th style="width:15%">Order Number</th>
								<th style="">Date Ordered</th>
								<th style="">Delivery Date</th>
								<th style="">Status</th>
								<th style="">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
						@forelse($orders as $order)
							<tr class="table-row">
								<td class="table-text">
									<h4>
										<a href="{{ url('customer/order').'/'.$order->order_id }}">#{{ $order->order_id }}</a>
									</h4>
								</td>          
								<td>
									<p>{{ date('M d, Y',strtotime($order->created_at)) }}</p>
								</td>   
								<td>
									<p>{{ date('M d, Y',strtotime($order->delivery_date)) }}</p>
								</td>   
								<td>
									<p>{{ ucwords($order->status) }}</p>
								</td>
								<td>
									<a href="{{ url('customer/order').'/'.$order->order_id }}" class="btn btn-primary btn-sm">View</a>
									@if($order->status == 'pending')
										<button data-id="{{ $order->id }}" class="cancel-btn btn btn-primary btn-sm">
											Cancel Order
										</button>
									@endif
								</td>
							</tr>
						@empty
							
						@endforelse                                    
						</tbody>
					</table>
				</div>
			</div>
		</div>
		</div>
	</div>

</div>

@endsection