@extends('master')

@section('title')  @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')<script>
$(document)
.on('click','#reply',function(e){
    e.preventDefault();
    swal({
        title: 'Send reply',
        html: "<div style='text-align:left;'>Message:</div><textarea class='swal2-input' style='height:200px;' id='message'></textarea>",
        showCancelButton: true,
        confirmButtonText: 'Submit',
        showLoaderOnConfirm: true,
        focusConfirm: true,
        onOpen: () => {
            
        },
        showLoaderOnConfirm: true,preConfirm: () => {
            return new Promise((resolve) => {
                setTimeout(() => {
                    var message = $("textarea#message").val();
                    if(message == ""){
                        swal.showValidationError('You cannot send an empty message')
                    }                    
                    resolve()
                }, 500)
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    }).then((result) => {
        if(result.value){
            $.post("/customer/inbox/reply",{
                'message' : $("textarea#message").val(),
                '_token' : "{{ csrf_token() }}",
                'message_id' : '{{ $id }}'
            },function(data){
                swal(data.swal).then(function(asdf){
                    if(asdf.value) location.reload()
                });
            });
        }
    });
})
;
</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<a href="{{ url('/customer') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
			<a href="{{ url('/customer/inbox') }}">Inbox</a>
			<i class="fa fa-angle-right"></i>
			<span>Message #{{ $id }}</span>
		</h2>
    </div>

	<div class="content-top">
		<div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>
			<div class="tab-pane active text-style" id="tab1">
				<div class="inbox-right">                                
					<div class="mailbox-content">
						<div class="clearfix">
							<div class='float-left'>
								<h2>
                                    {{ $messages[0]->title }}
								</h2>
							</div>
                            <div class="float-right">
                                <button class='btn btn-primary' id="reply"> <i class="fa fa-send"></i> Send Reply </button>
                            </div>
						</div>

						<hr>
                        <table class="table table-bordered table-striped table-hover datatable">
                            <tbody>
                            @forelse($messages as $message)
                                <tr class='table-row message-row' data-id="{{ $message->message_id }}">
                                    <td class='table-text'>
                                        <h6> {{ Sentinel::getUser()->id == $message->from ? "Me" : "Seller" }} : {{ $message->message }} </h6>
                                        <small> {{ date('M/d/Y h:i A',strtotime($message->created_at)) }} </small>
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>

					</div>
				</div>
			</div>
        </div>
	</div>

</div>

@endsection