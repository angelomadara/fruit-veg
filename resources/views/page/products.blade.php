@extends('page.page')

@section('slide')
    @include('page.slide')    
@endsection

@section('content')
<!-- gallery-->
<div id="gallery" class="gallery">
	<div class="container">
		<h3 class="title">Our Products</h3>
		<div class="gallery-info">
			@forelse($products as $product)
				<div class="col-md-4 col-sm-4 col-xs-4 gallery-grids">
					<img src="{{ url('images/small/').'/'.$product->photo }}" alt="{{ $product->name }}" />
					<div style="text-align:center;margin-bottom:2em;"> 
						{{ ucwords($product->name) }} - PHP {{ number_format($product->price,2) .'/'.$product->code }} 
						<!-- <div> <a href="#" style="color:blue"> +Add to cart</a> </div> -->
					</div>
					<!-- <a href="images/g1.jpg" class="gallery-box" data-lightbox="example-set" data-title="">
						<img src="images/g1.jpg" alt="" class="img-responsive zoom-img">
					</a> -->
				</div>
			@empty
			@endforelse
		</div>
	</div>
</div>
    
@endsection