@extends('page.page')

@section('slide')
    @include('page.slide')    
@endsection

@section('content')
<!-- contact-->
<div class="contact" id="contact">
		<div class="container">
			<h3 class="title clr">Contact</h3>
			<div class="contact-two-grids">
				<div class="col-md-8 contact-left-grid">
					<div class=" col-md-4 col-sm-5 contact-icons">
						<div class=" footer_grid_left">
							<div class="icon_grid_left">
								<span class="fa fa-map-marker" aria-hidden="true"></span>
							</div>
							<h5>Address</h5>
							<p>Balanga Public Market Brgy San Jose</span></p>
						</div>
						<div class=" footer_grid_left">

							<div class="icon_grid_left">
								<span class="fa fa-volume-control-phone" aria-hidden="true"></span>
							</div>
							<h5> Contact Us</h5>
							<p>09208486512 <span>2379848</span></p>
						</div>
						<div class=" footer_grid_left">
							<div class="icon_grid_left">

								<span class="fa fa-envelope" aria-hidden="true"></span>
							</div>
							<h5>Email Us</h5>
							<p><a href="mailto:info@example.com">info1@email.com</a>
								<span><a href="mailto:info@example.com">info2@email.com</a></span></p>
						</div>

					</div>
					<div class="col-md-8 col-sm-7 contact-us">
						@if(Session::has('success'))
							<div class='alert alert-success'> {{ Session::get('success') }} </div>
						@else
							<div class='alert alert-error'> {{ Session::get('error') }} </div>
						@endif
						<form action="{{url('contact-us/send')}}" method="post">
							@csrf
							<div class="styled-input">
								<input type="text" name="name" value="{{old('name')}}" placeholder="Name" required="">
							</div>
							<div class="styled-input">
								<input type="email" name="email" value="{{old('email')}}" placeholder="Email" required="">
							</div>
							<div class="styled-input">
								<input type="text" value="{{old('phone')}}" name="phone" placeholder="phone" required="">
							</div>
							<div class="styled-input">
								<textarea name="message" placeholder="Message" required="">{{ old('message') }}</textarea>
							</div>
							<div>
								<div class="click">
									<input type="submit" value="SEND">
								</div>
							</div>
						</form>
					</div>
					<div class="clearfix"> </div>
				</div>

				<div class="col-md-4 map-grid">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6509687.090753893!2d-123.76387427440008!3d37.18697025540024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808fb9fe5f285e3d%3A0x8b5109a227086f55!2sCalifornia%2C+USA!5e0!3m2!1sen!2sin!4v1491201047627"></iframe>
				</div>
				<div class="clearfix"> </div>
			</div>


		</div>
    </div>

@endsection