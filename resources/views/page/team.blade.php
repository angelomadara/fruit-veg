@extends('page.page')

@section('slide')
    @include('page.slide')    
@endsection

@section('content')
<!-- team-->
<div class="team agileits" id="team">
		<div class="team-info">
			<div class="container">
				<div class="team-row">
					<h3 class="title">Our Team</h3>
					<div class="col-md-3 col-sm-6 col-xs-6 team-grids">
						<div class="team-agile-img">
							<a href="#"><img src="images/home1.jpg" alt="img"></a>
							<div class="view-caption">
								<div class="w3ls-info">
									<h4> </h4>
								</div>
								<ul>
									<li><a href="#"><span class="fa fa-facebook"></span></a></li>
									<li><a href="#"><span class="fa fa-twitter"></span></a></li>
									<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 team-grids">
						<div class="team-agile-img">
							<a href="#"><img src="images/home2.jpg" style="width:100%" alt="img"></a>
							<div class="view-caption">
								<div class="w3ls-info">
									<h4>  </h4>
								</div>
								<ul>
									<li><a href="#"><span class="fa fa-facebook"></span></a></li>
									<li><a href="#"><span class="fa fa-twitter"></span></a></li>
									<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 team-grids">
						<div class="team-agile-img">
							<a href="#"><img src="images/home3.jpg" alt="img"></a>
							<div class="view-caption">
								<div class="w3ls-info">
									<h4> </h4>
								</div>
								<ul>
									<li><a href="#"><span class="fa fa-facebook"></span></a></li>
									<li><a href="#"><span class="fa fa-twitter"></span></a></li>
									<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 team-grids">
						<div class="team-agile-img">
							<a href="#"><img src="images/home4.jpg" alt="img"></a>
							<div class="view-caption">
								<div class="w3ls-info">
									<h4> </h4>
								</div>
								<ul>
									<li><a href="#"><span class="fa fa-facebook"></span></a></li>
									<li><a href="#"><span class="fa fa-twitter"></span></a></li>
									<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
	</div>
    <!-- //team-->
@endsection