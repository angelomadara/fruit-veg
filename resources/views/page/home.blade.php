@extends('page.page')

@section('slide')
    @include('page.slide')
@endsection

@section('content')
<div class="services" id="services">
		<div class="container">
			<h2 class="sub-title">Why Choose Us</h2>

			<div class="banner-bottom-girds">
				<div class="col-md-6  col-sm-6 col-xs-6  its-banner-grid">
					<div class="white-shadow">

						<div class=" col-md-8 white-right">
							<h4>Where do you get fruits and vegetables from?</h4>
							<p>Does your mom or dad (or aunt or grandma) buy
							them at the grocery store?”
							Give each child a chance to answer. Continue by saying,
							“There are a lot of places to get healthy fruits and
							vegetables. Have you ever picked an orange off a tree or
							picked strawberries off vines on the ground?”
							After giving each child a chance to answer, say, “There
							are a lot of places to get fruits and vegetables. We can
							buy them at the grocery store, at a fruit or vegetable
							stand (also called a produce stand), at a farmers’ market,
							or we can pick them from a garden. Has anybody ever
							gone to a farmers’ market?”</p>
							<div class="w3layouts_more-buttn ser-buttn">
								<a href="#" data-toggle="modal" data-target="#myModal">Read More</a>
							</div>

						</div>
						<div class="col-md-4 white-left">
							<span class="fa fa-truck banner-icon" aria-hidden="true"></span>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-6  col-sm-6 col-xs-6  its-banner-grid">
					<div class="white-shadow">

						<div class=" col-md-8 white-right">
							<h4>Best Quality</h4>
							<p>Like selecting the best fruit, being able to choose the freshest and tastiest vegetables is a combination of seasonal knowledge, asking farmers and shop owners for advice, and using your senses. This guide has the last part covered. Get ready to use your eyes, nose, and hands!</p>
							<div class="w3layouts_more-buttn ser-buttn">
								<a href="#" data-toggle="modal" data-target="#myModal">Read More</a>
							</div>

						</div>
						<div class="col-md-4 white-left">
							<span class="fa fa-flask banner-icon" aria-hidden="true"></span>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-6  col-sm-6 col-xs-6  its-banner-grid">
					<div class="white-shadow">

						<div class="col-md-8 white-right">
							<h4>Best Products</h4>
							<p>Some farmers, such as cash grain farmers or dairy farmers, have large, well-established markets. They can use existing organizations to perform the marketing function for them, or they can band together, form a cooperative, and market their products jointly. Small-scale fruit and vegetable growers generally have more difficulty finding established markets; therefore, they usually develop marketing systems tailored to their unique situations.</p>
							<div class="w3layouts_more-buttn ser-buttn">
								<a href="#" data-toggle="modal" data-target="#myModal">Read More</a>
							</div>

						</div>
						<div class="col-md-4 white-left">
							<span class="fa fa-pagelines banner-icon" aria-hidden="true"></span>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<!-- <div class="col-md-6  col-sm-6 col-xs-6  its-banner-grid">
					<div class="white-shadow">

						<div class=" col-md-8 white-right">
							<h4>Best Seeds</h4>
							<p>delectus reiciendis maiores alias consequatur aut.maiores alias consequatur aut.</p>
							<div class="w3layouts_more-buttn ser-buttn">
								<a href="#" data-toggle="modal" data-target="#myModal">Read More</a>
							</div>

						</div>
						<div class="col-md-4 white-left">
							<span class="fa fa-leaf banner-icon" aria-hidden="true"></span>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div> -->
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>

	<!--//Services-->

	<!-- modal -->
	<div class="modal about-modal fade" id="myModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Husbandry</h4>
				</div>
				<div class="modal-body">
					<div class="out-info">
						<img src="images/g1.jpg" alt="" />
						<p>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae,
							eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellu</p>
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- //modal -->
    
@endsection