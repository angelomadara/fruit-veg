@extends('page.page')

@section('slide')
    @include('page.slide')
@endsection

@section('content')
<br><br>
<!-- bolg -->
<div class="blog" id="blog">
		<div class="blog-two-grids">
			<h3 class="title">Our Services</h3>
			<div class="blog-top-grids">
				<div class="col-md-7 col-sm-7 blog-grid left-side">
					<h4>Lorem ipsum
					</h4>
					<p class="groom-right">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sed odio consequat, tristique elit sed, molestie nulla.
						Mauris et quam leo. Quisque tincidunt facilisis rutrum. Etiam mattis arcu vitae velit sagittis vehicula. Duis posuere
						ex in mollis iaculis.
					</p>
					<div class="w3layouts_more-buttn right-buttn">
						<a href="#" data-toggle="modal" data-target="#myModal">Read More</a>
					</div>
				</div>
				<div class="col-md-5 col-sm-5 blog-gr">
				</div>
				<div class="clearfix"> </div>

				<div class="blog-top-grids low-grid">
					<div class="col-md-6 col-sm-5 blog-br">
					</div>
					<div class="col-md-3 col-sm-4 blog-three">
						<div class="agri-matter">
							<p> Quisque tincidunt facilisis rutrum. Etiam mattis arcu vitae velit sagittis vehicula. Duis posuere ex in mollis iaculis.
							</p>
						</div>

					</div>
					<div class="col-md-3 col-sm-3  blog-fore">
					</div>
					<div class="clearfix"> </div>
				</div>

			</div>
		</div>
	</div>
    <!-- //blog -->
    
@endsection