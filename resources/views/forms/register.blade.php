
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title> Register </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="{{ url('css/bootstrap.min.css') }}" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="{{ url('css/style.css') }}" rel='stylesheet' type='text/css' />
<link href="{{ url('css/font-awesome.css') }}" rel="stylesheet"> 
<script src="{{ url('js/jquery.min.js') }}"> </script>
<script src="{{ url('js/bootstrap.min.js') }}"> </script>
</head>
<body>
	<div class="login">
		<h1>J&amp;A Online Ordering System</h1>
		<div class="login-bottom">
			<h2>Register</h2>
			<div class="col-lg-12">
			@if(Session::has('warning'))
				<div class='alert alert-warning'> {{ Session::get('warning') }} </div>
			@elseif(Session::has('success'))
				<div class='alert alert-success'> {{ Session::get('success') }} </div>
			@endif
			</div>
			<form method="POST" action="{{ url('security/register') }}">
			@csrf
			<div class="col-md-6">
                <div class="login-mail">
					<input type="text" name="email" placeholder="Email" required="">
					<i class="fa fa-envelope"></i>
				</div>
				<div class="login-mail">
					<input type="text" name="username" placeholder="Username" required="">
					<i class="fa fa-user"></i>
				</div>
				<div class="login-mail">
					<input type="password" name="password1" placeholder="Password" required="">
					<i class="fa fa-lock"></i>
				</div>
				<div class="login-mail">
					<input type="password" name="password2" placeholder="Repeated password" required="">
					<i class="fa fa-lock"></i>
				</div>
                <a class="news-letter " href="{{ url('security/forget-password') }}"> Forget Password</a>

			
			</div>
			<div class="col-md-6 login-do">
				<label class="hvr-shutter-in-horizontal login-sub">
					<input type="submit" value="Signup">
				</label>
				<p>Or</p>
				<a href="/login" class="hvr-shutter-in-horizontal">Login</a>
			</div>
			
			<div class="clearfix"> </div>
			</form>
		</div>
	</div>
		<!---->
<div class="copy-right">
            <p> &copy; 2016 Minimal. All Rights Reserved | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>	    </div>  
<!---->
<!--scrolling js-->
	<script src="{{ url('js/scripts.js"></script>
	<!--//scrolling js-->
</body>
</html>

