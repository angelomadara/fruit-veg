@extends('master')

@section('title') Inbox @endsection

@section('navs')
	@include('navs')
@endsection

@section('css')
@endsection

@section('js')
<script>
    $(document)
    .on("change","#category",function(){
        var category = $("#category option:selected").val();        
    })
</script>
@endsection

@section('content')

<div class="content-main">

    <div class="banner">		   
		<h2>
			<a href="{{ url('store/product/lists') }}">Products</a>
            <i class="fa fa-angle-right"></i>
			<span>Products</span>
		</h2>
    </div>

	<div class="content-top">
        <form action="{{ url('store/product-') }}{{ Request::get('action') ? 'update/'.Request::get('id') : 'create' }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>
                @if(Session::has('error'))
                    <div class='alert alert-danger'> {{ Session::get('error') }} </div>
                @elseif(Session::has('success'))
                    <div class='alert alert-success'> {{ Session::get('success') }} </div>
                @endif
            </div>
            @php
                $prod = (new App\Models\Products)->where('id',Request::get('id'))->first();
            @endphp
            <div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>
                <div class='grid-form1'>
                    <div class="row">
                        <div class='col-lg-12 col-md-12 col-sm-6 col-xs-6'>
                            <h3 class="pull-left">{{$prod ? "Update" : "Add"}} Product</h3>
                            <input type="submit" value="Save" class="btn btn-primary pull-right">
                        </div>
                        <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                            <div class="form-group">
                                <label for="" class="withAsterisk">Product Name</label>
                                <input type="text" class="form-control" name="name" id="name" value="{{ $prod ? $prod->name : null }}" placeholder="Product Name">
                            </div>
                            <div class="form-group">
                                <label for="" class="">Product Code</label>
                                <input type="text" class="form-control" name="code" id="code" value="{{ $prod ? $prod->code : null }}" placeholder="Product Code">
                            </div>
                            <div class="form-group">
                                <label for="" class="withAsterisk">Product Category</label>
                                <select name="category" id="category" class="form-control">
                                    <option value="" selected disabled></option>
                                @forelse($types as $type)
                                    <option value="{{ $type->id }}" {{$prod ? ($type->id == $prod->category_id ? "selected" : "") : ""}}> {{ ucwords($type->name) }} </option>
                                @empty 
                                    <option value="">No category found</option>
                                @endforelse
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="" class="withAsterisk">Price</label>
                                <input type="text" name="price" id="price" value="{{ $prod ? $prod->price : null }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="" class="withAsterisk">Per</label>
                                <select name="per" id="per" class="form-control">
                                    <option value="" selected disabled></option>
                                @forelse($per as $p)
                                    <option value="{{ $p->id }}" {{$prod ? ($p->id == $prod->per ? "selected" : "") : ""}}> {{ ucwords($p->name) }} </option>
                                @empty 
                                    
                                @endforelse
                                </select>
                            </div>                        
                            <div class="form-group">
                                <label for="" class="">Note</label>
                                <textarea name="note" id="note" cols="30" rows="10" class="form-control">{{ $prod ? $prod->note : null }}</textarea>
                            </div>
                        </div>
                        <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                            <div class="form-group">
                                <label for="" class="withAsterisk">Image</label>
                                <input type="file" name="image" id="image" class="form-control">
                                <img 
                                    src="{{ url('/images/small/'.($prod ? $prod->photo : null)) }}" 
                                    alt=""
                                    style="
                                        width:100%;
                                        margin-top:1em;
                                    "
                                >
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>

    </div>
</div>

@endsection