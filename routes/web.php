<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/

Auth::routes();

// Route::get('/', function () {
//     return view('page.home');
// });

Route::get('/','Page\HomeController@index');
Route::get('/index','Page\HomeController@index');
Route::get('/services','Page\ServicesController@index');
Route::get('/products','Page\ProductController@index');
Route::get('/who-we-are','Page\WhoWeAreController@index');
Route::get('/contact-us','Page\ContactController@index');
Route::post('/contact-us/send','Page\ContactController@sendEmail');

/** login routes */

Route::get('/login','Login\LoginController@index');
Route::post('security/login','Login\LoginController@authenticate');
Route::get('security/register','Login\LoginController@register');
Route::post('security/register','Login\LoginController@addUser');
Route::get('security/forget-password','Login\LoginController@forget');
Route::get('security/validate/user','Login\LoginController@reset');
Route::post('security/update-password','Login\LoginController@updatePassword');
Route::post('security/forget-password','Login\LoginController@reset');

Route::get('activate/{id}/{code}','Login\LoginController@activate');

/** Customer Routes */
Route::group(['prefix'=>'customer','middleware'=>'role:customer'],function(){
    Route::get('/','Customer\Home@index');
    Route::get('home','Customer\Home@index');
    
    Route::get('order-form','Customer\Order@index');
    Route::get('add-product/{id}','Customer\Order@addToCart');
    Route::post('send-order','Customer\Order@sendOrder');
    Route::get('order/{id}','Customer\Order@orderId');
    Route::get('order/{id}/update','Customer\Order@orderIdUpdate');
    Route::post('cancel-order','Customer\Order@cancelOrder');

    Route::get('inbox','Customer\Inbox@index');
    Route::post('inbox/create','Customer\Inbox@create');
    Route::get('inbox/{id}','Customer\Inbox@message');
    Route::post('inbox/reply','Customer\Inbox@reply');
    Route::get('inbox/remove/{id}','Customer\Inbox@remove');

    Route::get('settings','Customer\Settings@index');
    Route::get('settings/update','Customer\Settings@form');
    Route::post('settings/update','Customer\Settings@update');
    Route::post('settings/update/password','Customer\Settings@updatePassword');

    Route::get('profile/check','Customer\Settings@check');
});

/** Store Routes **/
Route::group(['prefix'=>'store','middleware'=>'role:owner'],function(){

    Route::get('/','Store\Home@index');
    Route::get('index','Store\Home@index');
    Route::get('report/week','Store\Home@thisWeek');

    Route::get('customer/orders','Store\Customer@index');
    Route::get('customer/order/approve/{order_id}','Store\Customer@approve');
    Route::get('customer/order/{id}/{order}','Store\Customer@orders');
    Route::post('credit','Store\Customer@credit');
    Route::get('delivered','Store\Customer@delivered');

    Route::get('customer/invoice','Store\Invoice@index');
    Route::get('customer/invoice/print/{id}','Store\Invoice@invoiceSamaSama');

    Route::get('product/lists','Store\Products@index');
    Route::get('product-form','Store\Products@form');
    Route::post('product-create','Store\Products@create');
    Route::post('product-update/{id}','Store\Products@update');
    Route::get('product-remove/{id}','Store\Products@remove');
    Route::post('product/quantity','Store\Products@quantity');

    Route::get('consolidated/orders','Store\Consolidated@orders');

    Route::get('calendar','Store\Calendar@index');
    Route::get('calendar/orders','Store\Calendar@getOrderCount');

    Route::get('reports','Store\Grap@index');
    Route::get('reports/monthly','Store\Grap@sales');

    Route::get('home','Store\Home@index');

    Route::get('inbox','Store\Inbox@index');
    Route::get('inbox/{id}','Store\Inbox@messageId');
    Route::post('inbox/reply','Store\Inbox@reply');
    Route::get('inbox/remove/{id}','Store\Inbox@remove');
    Route::post('inbox/announcement','Store\Inbox@announcement');

    Route::get('notification','Store\Notification@index');
    Route::get('orders','Store\Orders@index');

    Route::get('settings','Store\Settings@index');
    Route::post('settings/update-password','Store\Settings@updatePassword');

    Route::get('print/customer/order/{order}','Store\Printer@order');
    Route::get('print/consolidated/order','Store\Printer@consolidated');
});
// Address. Balanga Public Market Brgy San Jose

// Contact us
// 09208486512
// 2379848

// Email Us
// kahit ano nalang dito
// https://send.firefox.com/download/1aa588540f/#uuVjgdA3MSAXXj6tNBMmEw